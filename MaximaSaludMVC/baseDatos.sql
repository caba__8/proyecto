DROP DATABASE IF EXISTS maximaSalud;
--
CREATE DATABASE if not exists maximaSalud;
--
USE maximaSalud;
--
create table if not exists clientes(
    idcliente int auto_increment primary key,
    nombre varchar(50) not null,
    apellidos varchar(150) not null,
    dni varchar(10) not null,
    fechanacimiento date not null,
    fechaalta date not null,
    ciudad varchar(50) not null,
    pais varchar(50) not null);

-- Volcando datos para la tabla maximaSalud.clientes: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE clientes DISABLE KEYS */;
INSERT IGNORE INTO clientes (idcliente, nombre, apellidos, dni, fechanacimiento, fechaalta, ciudad, pais) VALUES
  (1, 'Jose', 'Juan', '77135722R', '1995-02-09', '2022-02-09', 'Zaragoza', 'Espana'),
  (7, 'Sergio', 'Perez', '22113254T', '1998-05-09', '2022-07-16', 'Valencia', 'Espana'),
  (8, 'Fernando', 'Alonso', '99233212U', '1987-04-25', '2022-05-19', 'Barcelona', 'Espana'),
  (9, 'Alberto', 'Garces', '31245768K', '1995-08-07', '2022-08-25', 'Valencia', 'Espana'),
  (10, 'Alejandro', 'Caballero', '73425637T', '1998-09-04', '2022-02-10', 'Zaragoza', 'Espana');
/*!40000 ALTER TABLE clientes ENABLE KEYS */;
--

create table if not exists tiendas (
    idtienda int auto_increment primary key,
    tienda varchar(50) not null,
    email varchar(100) not null,
    telefono varchar(9) not null,
    fechacreacion date not null,
    reputacion varchar(10) not null,
    web varchar(500) not null,
    ciudad varchar(100) not null);

-- Volcando datos para la tabla maximaSalud.tiendas: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE tiendas DISABLE KEYS */;
INSERT IGNORE INTO tiendas (idtienda, tienda, email, telefono, fechacreacion, reputacion, web, ciudad) VALUES
  (1, 'Farma', 'farma@gmail.com', 612354879, '2021-03-04', 'Buena', 'farma.com', 'Madrid'),
  (2, 'Altea', 'altea@hotmail.com', 613284597, '2022-03-04', 'Regular', 'altea.es', 'Barcelona'),
  (3, 'Medica', 'medica@gmail.com', 685497132, '2021-06-04', 'Mala', 'medica.es', 'Valencia'),
  (4, 'Teva', 'teva@hotmail.com', 654987321, '2021-08-04', 'Buena', 'teva.com', 'Zaragoza'),
  (5, 'Pinilla', 'pinilla@hotmail.com',	621453978, '1995-12-12', 'Buena', 'pinilla.com', 'Guadalajara'),
  (6, 'Herbo', 'herbo@gmail.com', 645123214, '2021-08-13', 'Regular', 'herbo.com', 'Sevilla'),
  (7, 'Molina', 'molina@outlook.com', 645214587, '2021-08-12', 'Buena', 'molina.es', 'Guadalajara'),
  (8, 'Megina', 'megina@hotmail.com', 636214532, '2022-07-05', 'Regular', 'megina.es', 'Guadalajara'),
  (9, 'Checa', 'checa@hotmail.com', 636521452, '2022-08-12', 'Regular', 'checa.es', 'Guadalajara');
--

create table if not exists productos(
    idproducto int auto_increment primary key,
    genero varchar(30) not null,
    codigo varchar(40) not null UNIQUE,
    idtienda int not null,
    idcliente int not null,
    precio float not null,
    fechaedicion date not null,
    fechacaducidad date not null,
    cantidad int not null);

-- Volcando datos para la tabla maximaSalud.productos: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE productos DISABLE KEYS */;
INSERT IGNORE INTO productos (idproducto, genero, codigo, idtienda, idcliente, precio, fechaedicion, fechacaducidad, cantidad) VALUES
  (1, 'Ibuprofeno', 'IBP', 1, 7, '12.14', '2021-03-04', '2024-03-04', 1000),
  (2, 'Flumil', 'FLM', 4, 1, '15', '2021-03-05', '2024-03-04', 900),
  (5, 'Paracetamol', 'PCT', 3, 8, '10.05', '2021-03-03', '2024-03-04', 550),
  (6, 'Dalsy', 'DSY', 2, 1, '8.95', '2021-03-04', '2024-03-04', 600),
  (7, 'Flumil', 'FP2', 2, 7, '5.81', '2022-08-03', '2022-08-18', 624),
  (8, 'Dalsy', 'DJ1', 1, 1, 12, '2022-08-23', '2022-08-27', 725),
  (9, 'Ebastel', 'EBJ1', 5, 7, 20, '2022-08-01', '2022-08-31', 855);
/*!40000 ALTER TABLE productos ENABLE KEYS */;
--

create table if not exists empleados(
    idempleado int auto_increment primary key,
    nombreE varchar(50) not null,
    apellidosE varchar(150) not null,
    dniE varchar(10) not null,
    fechanacimientoE date not null,
    idproducto int not null,
    idtienda int not null,
    paisE varchar(50) not null,
    sueldoE float not null);

-- Volcando datos para la tabla maximaSalud.empleados: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE empleados DISABLE KEYS */;
INSERT IGNORE INTO empleados (idempleado, nombreE, apellidosE, dniE, fechanacimientoE, idproducto, idtienda, paisE, sueldoE) VALUES
  (1, 'Jorge', 'Pombo', '31246578P', '1991-02-09', 1, 2, 'Espana', '2015.32'),
  (2, 'Sergio', 'Canales', '76451238C', '2000-02-10', 5, 6, 'Espana', '2150'),
  (3, 'Fernando', 'Alonso', '73412547T', '1993-08-10', 9, 5, 'Espana', '2850'),
  (4, 'Rafael', 'Nadal', '74125147R', '1985-04-15', 2, 7, 'Espana', '2150'),
  (5, 'Manuel', 'Molina', '74121453M', '1990-12-12', 6, 6, 'Espana', '1998.22'),
  (6, 'Alberto', 'Garcia', '71423564G', '1994-09-21', 2, 6, 'Espana', '2800');
/*!40000 ALTER TABLE empleados ENABLE KEYS */;
--

create table if not exists compras(
    idcompra int auto_increment primary key,
    codigoCompra varchar(10) not null,
    idcliente int not null,
    idtienda int not null,
    idproducto int not null,
    idempleado int not null,
    cantidadCompra int not null,
    fechacompra date not null);

--
create table if not exists USUARIO(
    NOMBRE_USER varchar(1000) not null,
    PW_USER varchar(9999) not null,
    EMAIL_USER varchar(999) not null,
    TELEFONO_USER varchar(999) not null);
--
alter table productos
    add foreign key (idtienda) references tiendas(idtienda),
    add foreign key (idcliente) references clientes(idcliente);
--
alter table empleados
    add foreign key (idproducto) references productos(idproducto),
    add foreign key (idtienda) references tiendas(idtienda);
--
alter table compras
    add foreign key (idcliente) references clientes(idcliente),
	add foreign key (idtienda) references tiendas(idtienda),
	add foreign key (idproducto) references productos(idproducto),
	add foreign key (idempleado) references empleados(idempleado);
--
delimiter ||
create function existeCodigo(f_codigo varchar(40))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idproducto) from productos)) do
    if  ((select codigo from productos where idproducto = (i + 1)) like f_codigo) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreTienda(f_name varchar(50))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idtienda) from tiendas)) do
    if  ((select tienda from tiendas where idtienda = (i + 1)) like f_name) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreCliente(f_name varchar(202))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idcliente) from clientes)) do
    if  ((select concat(apellidos, ', ', nombre) from clientes where idcliente = (i + 1)) like f_name) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreEmpleado(f_namE varchar(202))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idempleado) from empleados)) do
    if  ((select concat(apellidosE, ', ', nombreE) from empleados where idempleado = (i + 1)) like f_namE) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeCodigoCompra(f_codigoCompra varchar(40))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idcompra) from compras)) do
    if  ((select codigoCompra from compras where idcompra = (i + 1)) like f_codigoCompra) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreUsuario(f_NOMBRE_USER varchar(40))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(IDUSER) from USUARIO)) do
    if  ((select NOMBRE_USER from USUARIO where IDUSER = (i + 1)) like f_NOMBRE_USER) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;