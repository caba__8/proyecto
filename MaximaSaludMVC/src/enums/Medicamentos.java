package enums;

public enum Medicamentos {
    INFLAMACION("Ibuprofeno"),
    PARACETAMOL("Paracetamol"),
    ALERGIA("Ebastel"),
    MOCOS("Flumil"),
    PEQUES("Dalsy"),
    DEPRESION("Amitriptilina"),
    NEUMONIA("Amoxicilina"),
    BRONQUITIS("Azitromicina"),
    PANICO("Clonazepam"),
    HONGOS("Clotrimazol"),
    HIPERTENSION("Enalaptril"),
    INFECCIONES("Fluconazol"),
    ALERGIAS("Loratadina"),
    FIEBRE("Naproxeno"),
    ACIDEZ("Omeprazol"),
    INFLAMACIONES("Prednisona"),
    ASMA("Salbutamol"),
    FIEBRES("Aspirina"),
    CONGESTION("Couldina"),
    RESFRIADOS("Frenadol"),
    RESFRIADO("Strepsils"),
    TOS("Flutox"),
    GRIPE("Mucosan");

    private String valor;

    Medicamentos(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}