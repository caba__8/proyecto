package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Medicamentos;
import enums.Reputacion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/***
 * Clase Vista
 */
public class Vista extends JFrame {
    private final static String TITULOFRAME = "Maxima salud";
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JPanel jPanelProducto;
    private JPanel jPanelCliente;
    private JPanel jPanelTienda;
    private JPanel jPanelEmpleado;
    private JPanel jPanelCompra;

    //PRODUCTOS
    JComboBox comboCliente;
    JComboBox comboTienda;
    JComboBox comboGenero;
    DatePicker fecha;
    DatePicker fechaCaducidadProducto;
    JTextField txtCodigo;
    JTextField txtPrecio;
    JTextField txtCantidad;
    JTable productosTabla;
    JButton anadirProducto;
    JButton buscarProducto;
    JButton modificarProducto;
    JButton eliminarProducto;

    //CLIENTES
    JTextField txtNombre;
    JTextField txtApellidos;
    JTextField txtDni;
    DatePicker fechaNacimientoCliente;
    DatePicker fechaAlta;
    JTextField txtCiudadCliente;
    JTextField txtPais;
    JTable clientesTabla;
    JButton eliminarCliente;
    JButton buscarCliente;
    JButton anadirCliente;
    JButton modificarCliente;

    //TIENDAS
    JTextField txtNombreTienda;
    JTextField txtEmail;
    JTextField txtTelefono;
    JComboBox comboRepu;
    JTextField txtWeb;
    JTable tiendasTabla;
    JButton eliminarTienda;
    JButton buscarTienda;
    JButton anadirTienda;
    JButton modificarTienda;
    DatePicker fechaCreacionT;
    JTextField txtCiudadT;

    //EMPLEADOS
    JTextField txtNombreE;
    JTextField txtApellidosE;
    JTextField txtPaisE;
    JTextField txtDniE;
    DatePicker fechaNacimientoE;
    JComboBox comboBoxProductoEmp;
    JComboBox comboBoxTiendaEmp;
    JTextField txtSueldoE;
    JTable empleadosTabla;
    JButton anadirEmpleado;
    JButton buscarEmpleado;
    JButton modificarEmpleado;
    JButton eliminarEmpleado;

    //COMPRA
    JTable comprasTabla;
    JTextField txtCodigoCompra;
    JComboBox comboBoxCliente;
    JComboBox comboBoxProducto;
    JComboBox comboBoxTienda;
    JComboBox comboBoxEmpleado;
    JTextField txtCantidadCompra;
    DatePicker fechaCompra;
    JButton anadirCompra;
    JButton buscarCompra;
    JButton modificarCompra;
    JButton eliminarCompra;

    //BUSQUEDA
    JLabel etiquetaEstado;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmTiendas;
    DefaultTableModel dtmClientes;
    DefaultTableModel dtmProductos;
    DefaultTableModel dtmEmpleados;
    DefaultTableModel dtmCompras;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemCrearBBDD;
    JMenuItem itemBorrarBBDD;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    //CUADRO DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /***
     * Constructor
     */
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    /***
     * Iniciar la ventana
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);
        //creo cuadro de dialogo
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /***
     * Establecer los TableModels
     */
    private void setTableModels() {
        this.dtmProductos = new DefaultTableModel();
        this.productosTabla.setModel(dtmProductos);
        this.dtmClientes = new DefaultTableModel();
        this.clientesTabla.setModel(dtmClientes);
        this.dtmTiendas = new DefaultTableModel();
        this.tiendasTabla.setModel(dtmTiendas);
        this.dtmEmpleados = new DefaultTableModel();
        this.empleadosTabla.setModel(dtmEmpleados);
        this.dtmCompras = new DefaultTableModel();
        this.comprasTabla.setModel(dtmCompras);
        this.clientesTabla.setBackground(Color.WHITE);
        this.productosTabla.setBackground(Color.WHITE);
        this.tiendasTabla.setBackground(Color.WHITE);
        this.empleadosTabla.setBackground(Color.WHITE);
        this.comprasTabla.setBackground(Color.WHITE);
    }

    /***
     * Establecer el menú
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        //por cada item que tenga funcionalidad tiene un ActionCommand
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemCrearBBDD = new JMenuItem("Crear base de datos");
        itemCrearBBDD.setActionCommand("CrearBBDD");
        itemBorrarBBDD = new JMenuItem("Borrar base de datos");
        itemBorrarBBDD.setActionCommand("BorrarBBDD");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemCrearBBDD);
        menu.add(itemBorrarBBDD);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        //centrar en horizontal
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /***
     * Establecer los ComboBox
     */
    private void setEnumComboBox() {
        for (Reputacion constant : Reputacion.values()) {
            comboRepu.addItem(constant.getValor());
        }
        comboRepu.setSelectedIndex(-1);
        for (Medicamentos constant : Medicamentos.values()) {
            comboGenero.addItem(constant.getValor());
        }
        comboGenero.setSelectedIndex(-1);
    }

    /***
     * Establecer el cuadro de diálogo de administrador
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[]{adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);
        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}