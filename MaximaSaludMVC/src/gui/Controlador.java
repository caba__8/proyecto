package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/***
 * Clase Controlador
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    private boolean refrescar;
    private boolean existeTabla;

    /***
     * Constructor
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        existeTabla = modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        if (existeTabla) {
            refrescarTodo();
        }
    }

    private void refrescarTodo() {
        refrescarClientes();
        refrescarTienda();
        refrescarProductos();
        refrescarEmpleados();
        refrescarCompras();
        refrescar = false;
    }

    /***
     * Añadir Listeners
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.anadirProducto.addActionListener(listener);
        vista.anadirProducto.setActionCommand("anadirProducto");
        vista.buscarProducto.addActionListener(listener);
        vista.buscarProducto.setActionCommand("buscarProducto");
        vista.anadirCliente.addActionListener(listener);
        vista.anadirCliente.setActionCommand("anadirCliente");
        vista.buscarCliente.addActionListener(listener);
        vista.buscarCliente.setActionCommand("buscarCliente");
        vista.anadirTienda.addActionListener(listener);
        vista.anadirTienda.setActionCommand("anadirTienda");
        vista.buscarTienda.addActionListener(listener);
        vista.buscarTienda.setActionCommand("buscarTienda");
        vista.anadirEmpleado.addActionListener(listener);
        vista.anadirEmpleado.setActionCommand("anadirEmpleado");
        vista.buscarEmpleado.addActionListener(listener);
        vista.buscarEmpleado.setActionCommand("buscarEmpleado");
        vista.anadirCompra.addActionListener(listener);
        vista.anadirCompra.setActionCommand("anadirCompra");
        vista.buscarCompra.addActionListener(listener);
        vista.buscarCompra.setActionCommand("buscarCompra");
        vista.eliminarProducto.addActionListener(listener);
        vista.eliminarProducto.setActionCommand("eliminarProducto");
        vista.eliminarCliente.addActionListener(listener);
        vista.eliminarCliente.setActionCommand("eliminarCliente");
        vista.eliminarTienda.addActionListener(listener);
        vista.eliminarTienda.setActionCommand("eliminarTienda");
        vista.eliminarEmpleado.addActionListener(listener);
        vista.eliminarEmpleado.setActionCommand("eliminarEmpleado");
        vista.eliminarCompra.addActionListener(listener);
        vista.eliminarCompra.setActionCommand("eliminarCompra");
        vista.modificarProducto.addActionListener(listener);
        vista.modificarProducto.setActionCommand("modificarProducto");
        vista.modificarCliente.addActionListener(listener);
        vista.modificarCliente.setActionCommand("modificarCliente");
        vista.modificarTienda.addActionListener(listener);
        vista.modificarTienda.setActionCommand("modificarTienda");
        vista.modificarEmpleado.addActionListener(listener);
        vista.modificarEmpleado.setActionCommand("modificarEmpleado");
        vista.modificarCompra.addActionListener(listener);
        vista.modificarCompra.setActionCommand("modificarCompra");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemCrearBBDD.addActionListener(listener);
        vista.itemBorrarBBDD.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tiendasTabla.getSelectionModel())) {
                int row = vista.tiendasTabla.getSelectedRow();
                vista.txtNombreTienda.setText(String.valueOf(vista.tiendasTabla.getValueAt(row, 1)));
                vista.txtEmail.setText(String.valueOf(vista.tiendasTabla.getValueAt(row, 2)));
                vista.txtTelefono.setText(String.valueOf(vista.tiendasTabla.getValueAt(row, 3)));
                vista.fechaCreacionT.setDate((Date.valueOf(String.valueOf(vista.tiendasTabla.getValueAt(row, 4)))).toLocalDate());
                vista.comboRepu.setSelectedItem(String.valueOf(vista.tiendasTabla.getValueAt(row, 5)));
                vista.txtWeb.setText(String.valueOf(vista.tiendasTabla.getValueAt(row, 6)));
                vista.txtCiudadT.setText(String.valueOf(vista.tiendasTabla.getValueAt(row, 7)));
            } else if (e.getSource().equals(vista.clientesTabla.getSelectionModel())) {
                int row = vista.clientesTabla.getSelectedRow();
                vista.txtNombre.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 1)));
                vista.txtApellidos.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 2)));
                vista.txtDni.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 3)));
                vista.fechaNacimientoCliente.setDate((Date.valueOf(String.valueOf(vista.clientesTabla.getValueAt(row, 4)))).toLocalDate());
                vista.fechaAlta.setDate((Date.valueOf(String.valueOf(vista.clientesTabla.getValueAt(row, 5)))).toLocalDate());
                vista.txtCiudadCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 6)));
                vista.txtPais.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 7)));
            } else if (e.getSource().equals(vista.productosTabla.getSelectionModel())) {
                int row = vista.productosTabla.getSelectedRow();
                vista.comboGenero.setSelectedItem(String.valueOf(vista.productosTabla.getValueAt(row, 1)));
                vista.comboCliente.setSelectedItem(String.valueOf(vista.productosTabla.getValueAt(row, 4)));
                vista.comboTienda.setSelectedItem(String.valueOf(vista.productosTabla.getValueAt(row, 3)));
                vista.fechaCaducidadProducto.setDate((Date.valueOf(String.valueOf(vista.productosTabla.getValueAt(row, 7)))).toLocalDate());
                vista.fecha.setDate((Date.valueOf(String.valueOf(vista.productosTabla.getValueAt(row, 6)))).toLocalDate());
                vista.txtCodigo.setText(String.valueOf(vista.productosTabla.getValueAt(row, 2)));
                vista.txtPrecio.setText(String.valueOf(vista.productosTabla.getValueAt(row, 5)));
                vista.txtCantidad.setText(String.valueOf(vista.productosTabla.getValueAt(row, 8)));
            } else if (e.getSource().equals(vista.empleadosTabla.getSelectionModel())) {
                int row = vista.empleadosTabla.getSelectedRow();
                vista.txtNombreE.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 1)));
                vista.txtApellidosE.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 2)));
                vista.txtDniE.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 3)));
                vista.fechaNacimientoE.setDate((Date.valueOf(String.valueOf(vista.empleadosTabla.getValueAt(row, 4)))).toLocalDate());
                vista.comboBoxProductoEmp.setSelectedItem(String.valueOf(vista.empleadosTabla.getValueAt(row, 5)));
                vista.comboBoxTiendaEmp.setSelectedItem(String.valueOf(vista.empleadosTabla.getValueAt(row, 6)));
                vista.txtPaisE.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 7)));
                vista.txtSueldoE.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 8)));
            } else if (e.getSource().equals(vista.comprasTabla.getSelectionModel())) {
                int row = vista.comprasTabla.getSelectedRow();
                vista.txtCodigoCompra.setText(String.valueOf(vista.comprasTabla.getValueAt(row, 1)));
                vista.comboBoxProducto.setSelectedItem(String.valueOf(vista.comprasTabla.getValueAt(row, 2)));
                vista.comboBoxCliente.setSelectedItem(String.valueOf(vista.comprasTabla.getValueAt(row, 3)));
                vista.comboBoxTienda.setSelectedItem(String.valueOf(vista.comprasTabla.getValueAt(row, 4)));
                vista.comboBoxEmpleado.setSelectedItem(String.valueOf(vista.comprasTabla.getValueAt(row, 5)));
                vista.txtCantidadCompra.setText(String.valueOf(vista.comprasTabla.getValueAt(row, 6)));
                vista.fechaCompra.setDate((Date.valueOf(String.valueOf(vista.comprasTabla.getValueAt(row, 7)))).toLocalDate());
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tiendasTabla.getSelectionModel())) {
                    borrarCamposTiendas();
                } else if (e.getSource().equals(vista.clientesTabla.getSelectionModel())) {
                    borrarCamposClientes();
                } else if (e.getSource().equals(vista.productosTabla.getSelectionModel())) {
                    borrarCamposProductos();
                } else if (e.getSource().equals(vista.empleadosTabla.getSelectionModel())) {
                    borrarCamposEmpleados();
                } else if (e.getSource().equals(vista.comprasTabla.getSelectionModel())) {
                    borrarCamposCompras();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (!existeTabla && !command.equals("CrearBBDD")) {
            JOptionPane.showMessageDialog(null, "Debes crearla desde el menú opciones",
                    "No existe la base de datos maximaSalud", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "CrearBBDD":
                existeTabla = modelo.crearBBDD();
                break;
            case "BorrarBBDD":
                if (modelo.borrarBBDD()) {
                    existeTabla = false;
                }
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "anadirProducto":
                try {
                    if (comprobarProductoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.productosTabla.clearSelection();
                    } else if (modelo.productoCodigoYaExiste(vista.txtCodigo.getText())) {
                        Util.showErrorAlert("Ese codigo ya existe");
                        vista.productosTabla.clearSelection();
                    } else {
                        modelo.insertarProducto(
                                String.valueOf(vista.comboGenero.getSelectedItem()),
                                vista.txtCodigo.getText(),
                                String.valueOf(vista.comboTienda.getSelectedItem()),
                                String.valueOf(vista.comboCliente.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fecha.getDate(),
                                vista.fechaCaducidadProducto.getDate(),
                                Integer.parseInt(vista.txtCantidad.getText()));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.productosTabla.clearSelection();
                }
                borrarCamposProductos();
                refrescarProductos();
                break;
            case "buscarProducto":
                try {
                    if (vista.txtCodigo.getText().length() == 0) {
                        JOptionPane.showMessageDialog(null, "Escribe el codigo del producto que quieres buscar",
                                "Campo codigo vacío", JOptionPane.PLAIN_MESSAGE);
                        refrescarProductos();
                    } else {
                        vista.productosTabla.setModel(construirTableModelProductos(modelo.buscarProducto(vista.txtCodigo.getText())));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "modificarProducto":
                try {
                    if (comprobarProductoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.productosTabla.clearSelection();
                    } else {
                        modelo.modificarProducto(
                                String.valueOf(vista.comboGenero.getSelectedItem()),
                                vista.txtCodigo.getText(),
                                String.valueOf(vista.comboTienda.getSelectedItem()),
                                String.valueOf(vista.comboCliente.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fecha.getDate(),
                                vista.fechaCaducidadProducto.getDate(),
                                Integer.parseInt(vista.txtCantidad.getText()),
                                Integer.parseInt((String) vista.productosTabla.getValueAt(vista.productosTabla.getSelectedRow(), 0))
                        );
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.productosTabla.clearSelection();
                }
                borrarCamposProductos();
                refrescarProductos();
                break;
            case "eliminarProducto":
                modelo.borrarProducto(Integer.parseInt((String) vista.productosTabla.getValueAt(vista.productosTabla.getSelectedRow(), 0)));
                borrarCamposProductos();
                refrescarProductos();
                break;
            case "anadirCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else if (modelo.clienteNombreYaExiste(vista.txtNombre.getText(),
                            vista.txtApellidos.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un cliente diferente");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.insertarCliente(vista.txtNombre.getText(),
                                vista.txtApellidos.getText(),
                                vista.txtDni.getText(),
                                vista.fechaNacimientoCliente.getDate(),
                                vista.fechaAlta.getDate(),
                                vista.txtCiudadCliente.getText(),
                                vista.txtPais.getText());
                        refrescarClientes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
            }
            break;
            case "buscarCliente": {
                try {
                    if (vista.txtDni.getText().length() == 0) {
                        JOptionPane.showMessageDialog(null, "Escribe el DNI del cliente que quieres buscar",
                                "Campo DNI vacío", JOptionPane.PLAIN_MESSAGE);
                        refrescarClientes();
                    } else {
                        vista.clientesTabla.setModel(construirTableModeloClientes(modelo.buscarCliente(vista.txtDni.getText())));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            break;
            case "modificarCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.modificarCliente(vista.txtNombre.getText(), vista.txtApellidos.getText(),
                                vista.txtDni.getText(), vista.fechaNacimientoCliente.getDate(), vista.fechaAlta.getDate(),
                                vista.txtCiudadCliente.getText(), vista.txtPais.getText(),
                                Integer.parseInt((String) vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                        refrescarClientes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
            }
            break;
            case "eliminarCliente":
                modelo.borrarCliente(Integer.parseInt((String) vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                borrarCamposClientes();
                refrescarClientes();
                break;
            case "anadirTienda": {
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tiendasTabla.clearSelection();
                    } else if (modelo.tiendaNombreYaExiste(vista.txtNombreTienda.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una tienda diferente.");
                        vista.tiendasTabla.clearSelection();
                    } else {
                        modelo.insertarTienda(vista.txtNombreTienda.getText(), vista.txtEmail.getText(),
                                vista.txtTelefono.getText(), vista.fechaCreacionT.getDate(),
                                (String) vista.comboRepu.getSelectedItem(),
                                vista.txtWeb.getText(), vista.txtCiudadT.getText());
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tiendasTabla.clearSelection();
                }
                borrarCamposTiendas();
            }
            break;
            case "buscarTienda": {
                try {
                    if (vista.txtNombreTienda.getText().length() == 0) {
                        JOptionPane.showMessageDialog(null, "Escribe el nombre de la tienda que quieres buscar",
                                "El nombre de la tienda está vacío", JOptionPane.PLAIN_MESSAGE);
                        refrescarTienda();
                    } else {
                        vista.tiendasTabla.setModel(construirTableModelTiendas(modelo.buscarTienda(vista.txtNombreTienda.getText())));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            break;
            case "modificarTienda": {
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tiendasTabla.clearSelection();
                    } else {
                        modelo.modificarTienda(vista.txtNombreTienda.getText(), vista.txtEmail.getText(), vista.txtTelefono.getText(),
                                vista.fechaCreacionT.getDate(), String.valueOf(vista.comboRepu.getSelectedItem()), vista.txtWeb.getText(), vista.txtCiudadT.getText(),
                                Integer.parseInt((String) vista.tiendasTabla.getValueAt(vista.tiendasTabla.getSelectedRow(), 0)));
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tiendasTabla.clearSelection();
                }
                borrarCamposTiendas();
            }
            break;
            case "eliminarTienda":
                modelo.borrarTienda(Integer.parseInt((String) vista.tiendasTabla.getValueAt(vista.tiendasTabla.getSelectedRow(), 0)));
                borrarCamposTiendas();
                refrescarTienda();
                break;
            case "anadirEmpleado": {
                try {
                    if (comprobarEmpleadoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empleadosTabla.clearSelection();
                    } else if (modelo.empleadoNombreYaExiste(vista.txtNombreE.getText(),
                            vista.txtApellidosE.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un empleado diferente");
                        vista.empleadosTabla.clearSelection();
                    } else {
                        modelo.insertarEmpleado(vista.txtNombreE.getText(),
                                vista.txtApellidosE.getText(),
                                vista.txtDniE.getText(),
                                vista.fechaNacimientoE.getDate(),
                                String.valueOf(vista.comboBoxProductoEmp.getSelectedItem()),
                                String.valueOf(vista.comboBoxTiendaEmp.getSelectedItem()),
                                vista.txtPaisE.getText(),
                                Float.parseFloat(vista.txtSueldoE.getText()));
                        refrescarEmpleados();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.empleadosTabla.clearSelection();
                }
                borrarCamposEmpleados();
            }
            break;
            case "buscarEmpleado": {
                try {
                    if (vista.txtDniE.getText().length() == 0) {
                        JOptionPane.showMessageDialog(null, "Escribe el DNI del empleado que quieres buscar",
                                "Campo DNI vacío", JOptionPane.PLAIN_MESSAGE);
                        refrescarEmpleados();
                    } else {
                        vista.empleadosTabla.setModel(construirTableModeloEmpleados(modelo.buscarEmpleado(vista.txtDniE.getText())));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            break;
            case "modificarEmpleado": {
                try {
                    if (comprobarEmpleadoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empleadosTabla.clearSelection();
                    } else {
                        modelo.modificarEmpleado(vista.txtNombreE.getText(), vista.txtApellidosE.getText(),
                                vista.txtDniE.getText(), vista.fechaNacimientoE.getDate(), String.valueOf(vista.comboBoxProductoEmp.getSelectedItem()),
                                String.valueOf(vista.comboBoxTiendaEmp.getSelectedItem()), vista.txtPaisE.getText(), Float.parseFloat(vista.txtSueldoE.getText()),
                                Integer.parseInt((String) vista.empleadosTabla.getValueAt(vista.empleadosTabla.getSelectedRow(), 0)));
                        refrescarEmpleados();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.empleadosTabla.clearSelection();
                }
                borrarCamposEmpleados();
            }
            break;
            case "eliminarEmpleado":
                modelo.borrarEmpleado(Integer.parseInt((String) vista.empleadosTabla.getValueAt(vista.empleadosTabla.getSelectedRow(), 0)));
                borrarCamposEmpleados();
                refrescarEmpleados();
                break;
            case "anadirCompra":
                try {
                    if (comprobarCompraVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.comprasTabla.clearSelection();
                    } else if (modelo.compraCodigoYaExiste(vista.txtCodigoCompra.getText())) {
                        Util.showErrorAlert("Ese codigo ya existe");
                        vista.comprasTabla.clearSelection();
                    } else {
                        modelo.insertarCompra(
                                vista.txtCodigoCompra.getText(),
                                String.valueOf(vista.comboBoxProducto.getSelectedItem()),
                                String.valueOf(vista.comboBoxCliente.getSelectedItem()),
                                String.valueOf(vista.comboBoxTienda.getSelectedItem()),
                                String.valueOf(vista.comboBoxEmpleado.getSelectedItem()),
                                Integer.parseInt(vista.txtCantidadCompra.getText()),
                                vista.fechaCompra.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.comprasTabla.clearSelection();
                }
                borrarCamposCompras();
                refrescarCompras();
                break;
            case "buscarCompra":
                try {
                    if (vista.txtCodigoCompra.getText().length() == 0) {
                        JOptionPane.showMessageDialog(null, "Escribe el codigo de la compra que quieres buscar",
                                "Campo codigo vacío", JOptionPane.PLAIN_MESSAGE);
                        refrescarCompras();
                    } else {
                        vista.comprasTabla.setModel(construirTableModelCompras(modelo.buscarCompra(vista.txtCodigoCompra.getText())));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "modificarCompra":
                try {
                    if (comprobarCompraVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.comprasTabla.clearSelection();
                    } else {
                        modelo.modificarCompra(
                                vista.txtCodigoCompra.getText(),
                                String.valueOf(vista.comboBoxProducto.getSelectedItem()),
                                String.valueOf(vista.comboBoxCliente.getSelectedItem()),
                                String.valueOf(vista.comboBoxTienda.getSelectedItem()),
                                String.valueOf(vista.comboBoxEmpleado.getSelectedItem()),
                                Integer.parseInt(vista.txtCantidadCompra.getText()),
                                vista.fechaCompra.getDate(),
                                Integer.parseInt((String) vista.comprasTabla.getValueAt(vista.comprasTabla.getSelectedRow(), 0))
                        );
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.comprasTabla.clearSelection();
                }
                borrarCamposCompras();
                refrescarCompras();
                break;
            case "eliminarCompra":
                modelo.borrarCompra(Integer.parseInt((String) vista.comprasTabla.getValueAt(vista.comprasTabla.getSelectedRow(), 0)));
                borrarCamposCompras();
                refrescarCompras();
                break;
        }
    }

    private void refrescarTienda() {
        try {
            vista.tiendasTabla.setModel(construirTableModelTiendas(modelo.consultarTienda()));
            vista.comboTienda.removeAllItems();
            vista.comboBoxTienda.removeAllItems();
            vista.comboBoxTiendaEmp.removeAllItems();
            for (int i = 0; i < vista.dtmTiendas.getRowCount(); i++) {
                vista.comboBoxTienda.addItem(vista.dtmTiendas.getValueAt(i, 0) + " - " +
                        vista.dtmTiendas.getValueAt(i, 1));
            }
            for (int i = 0; i < vista.dtmTiendas.getRowCount(); i++) {
                vista.comboTienda.addItem(vista.dtmTiendas.getValueAt(i, 0) + " - " +
                        vista.dtmTiendas.getValueAt(i, 1));
            }
            for (int i = 0; i < vista.dtmTiendas.getRowCount(); i++) {
                vista.comboBoxTiendaEmp.addItem(vista.dtmTiendas.getValueAt(i, 0) + " - " +
                        vista.dtmTiendas.getValueAt(i, 1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelTiendas(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmTiendas.setDataVector(data, columnNames);
        return vista.dtmTiendas;
    }

    private void refrescarClientes() {
        try {
            vista.clientesTabla.setModel(construirTableModeloClientes(modelo.consultarCliente()));
            vista.comboCliente.removeAllItems();
            vista.comboBoxCliente.removeAllItems();
            for (int i = 0; i < vista.dtmClientes.getRowCount(); i++) {
                vista.comboCliente.addItem(vista.dtmClientes.getValueAt(i, 0) + " - " +
                        vista.dtmClientes.getValueAt(i, 2) + ", " + vista.dtmClientes.getValueAt(i, 1));
            }
            vista.comboBoxCliente.removeAllItems();
            for (int i = 0; i < vista.dtmClientes.getRowCount(); i++) {
                vista.comboBoxCliente.addItem(vista.dtmClientes.getValueAt(i, 0) + " - " +
                        vista.dtmClientes.getValueAt(i, 2) + ", " + vista.dtmClientes.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloClientes(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmClientes.setDataVector(data, columnNames);
        return vista.dtmClientes;
    }

    /**
     * Actualiza los productos que se ven en la lista y los comboboxes
     */
    private void refrescarProductos() {
        try {
            vista.productosTabla.setModel(construirTableModelProductos(modelo.consultarProductos()));
            vista.comboBoxProducto.removeAllItems();
            vista.comboBoxProductoEmp.removeAllItems();
            for (int i = 0; i < vista.dtmProductos.getRowCount(); i++) {
                vista.comboBoxProducto.addItem(vista.dtmProductos.getValueAt(i, 0) + " - " +
                        vista.dtmProductos.getValueAt(i, 2) + ", " + vista.dtmProductos.getValueAt(i, 1));
            }
            for (int i = 0; i < vista.dtmProductos.getRowCount(); i++) {
                vista.comboBoxProductoEmp.addItem(vista.dtmProductos.getValueAt(i, 0) + " - " +
                        vista.dtmProductos.getValueAt(i, 2) + ", " + vista.dtmProductos.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelProductos(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmProductos.setDataVector(data, columnNames);
        return vista.dtmProductos;
    }

    private void refrescarEmpleados() {
        try {
            vista.empleadosTabla.setModel(construirTableModeloEmpleados(modelo.consultarEmpleado()));
            vista.comboBoxEmpleado.removeAllItems();
            for (int i = 0; i < vista.dtmEmpleados.getRowCount(); i++) {
                vista.comboBoxEmpleado.addItem(vista.dtmEmpleados.getValueAt(i, 0) + " - " +
                        vista.dtmEmpleados.getValueAt(i, 2) + ", " + vista.dtmEmpleados.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloEmpleados(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmEmpleados.setDataVector(data, columnNames);
        return vista.dtmEmpleados;
    }

    private void refrescarCompras() {
        try {
            vista.comprasTabla.setModel(construirTableModelCompras(modelo.consultarCompras()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelCompras(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmCompras.setDataVector(data, columnNames);
        return vista.dtmCompras;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void setOptions() {
    }

    private void borrarCamposProductos() {
        vista.comboTienda.setSelectedIndex(-1);
        vista.comboCliente.setSelectedIndex(-1);
        vista.comboGenero.setSelectedIndex(-1);
        vista.txtCodigo.setText("");
        vista.txtPrecio.setText("");
        vista.fecha.setText("");
        vista.fechaCaducidadProducto.setText("");
        vista.txtCantidad.setText("");
    }

    private void borrarCamposClientes() {
        vista.txtNombre.setText("");
        vista.txtApellidos.setText("");
        vista.txtDni.setText("");
        vista.txtCiudadCliente.setText("");
        vista.txtPais.setText("");
        vista.fechaNacimientoCliente.setText("");
        vista.fechaAlta.setText("");
    }

    private void borrarCamposTiendas() {
        vista.txtNombreTienda.setText("");
        vista.txtEmail.setText("");
        vista.txtTelefono.setText("");
        vista.fechaCreacionT.setText("");
        vista.comboRepu.setSelectedIndex(-1);
        vista.txtWeb.setText("");
        vista.txtCiudadT.setText("");
    }

    private void borrarCamposEmpleados() {
        vista.txtNombreE.setText("");
        vista.txtApellidosE.setText("");
        vista.txtDniE.setText("");
        vista.fechaNacimientoE.setText("");
        vista.txtPaisE.setText("");
        vista.comboBoxProductoEmp.setSelectedIndex(-1);
        vista.comboBoxTiendaEmp.setSelectedIndex(-1);
        vista.txtSueldoE.setText("");
    }

    private void borrarCamposCompras() {
        vista.txtCodigoCompra.setText("");
        vista.comboBoxProducto.setSelectedIndex(-1);
        vista.comboBoxCliente.setSelectedIndex(-1);
        vista.comboBoxTienda.setSelectedIndex(-1);
        vista.comboBoxEmpleado.setSelectedIndex(-1);
        vista.txtCantidadCompra.setText("");
        vista.fechaCompra.setText("");
    }

    private boolean comprobarProductoVacio() {
        return vista.comboGenero.getSelectedIndex() == -1 ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.txtCodigo.getText().isEmpty() ||
                vista.comboCliente.getSelectedIndex() == -1 ||
                vista.comboTienda.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty() ||
                vista.fechaCaducidadProducto.getText().isEmpty() ||
                vista.txtCantidad.getText().isEmpty();
    }

    private boolean comprobarClienteVacio() {
        return vista.txtApellidos.getText().isEmpty() ||
                vista.txtNombre.getText().isEmpty() ||
                vista.txtPais.getText().isEmpty() ||
                vista.txtDni.getText().isEmpty() ||
                vista.txtCiudadCliente.getText().isEmpty() ||
                vista.fechaNacimientoCliente.getText().isEmpty() ||
                vista.fechaAlta.getText().isEmpty();
    }

    private boolean comprobarTiendaVacia() {
        return vista.txtNombreTienda.getText().isEmpty() ||
                vista.txtEmail.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.fechaCreacionT.getText().isEmpty() ||
                vista.comboRepu.getSelectedIndex() == -1 ||
                vista.txtWeb.getText().isEmpty() ||
                vista.txtCiudadT.getText().isEmpty();
    }

    private boolean comprobarEmpleadoVacio() {
        return vista.txtApellidosE.getText().isEmpty() ||
                vista.txtNombreE.getText().isEmpty() ||
                vista.txtDniE.getText().isEmpty() ||
                vista.txtPaisE.getText().isEmpty() ||
                vista.fechaNacimientoE.getText().isEmpty() ||
                vista.comboBoxProductoEmp.getSelectedIndex() == -1 ||
                vista.comboBoxTiendaEmp.getSelectedIndex() == -1 ||
                vista.txtSueldoE.getText().isEmpty();
    }

    private boolean comprobarCompraVacia() {
        return vista.txtCodigoCompra.getText().isEmpty() ||
                vista.comboBoxProducto.getSelectedIndex() == -1 ||
                vista.comboBoxCliente.getSelectedIndex() == -1 ||
                vista.comboBoxTienda.getSelectedIndex() == -1 ||
                vista.comboBoxEmpleado.getSelectedIndex() == -1 ||
                vista.txtCantidadCompra.getText().isEmpty() ||
                vista.fechaCompra.getText().isEmpty();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}