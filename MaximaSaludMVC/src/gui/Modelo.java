package gui;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Clase Modelo
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    /***
     * Conectar con la BBDD
     * @return
     */
    boolean conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"
                    + ip + ":3306/maximaSalud", user, password);
            return true;
        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Puedes crearla desde el menú opciones",
                    "No se ha encontrado la base de datos", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
    }

    /***
     * Crear la BBDD
     * @return
     */
    boolean crearBBDD() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"
                    + ip + ":3306/", user, password);
            PreparedStatement statement = null;

            String code = leerFichero();
            String[] query = code.split("--");
            for (String aQuery : query) {
                statement = conexion.prepareStatement(aQuery);
                statement.executeUpdate();
            }
            assert statement != null;
            statement.close();
            JOptionPane.showMessageDialog(null, "Base de datos creada correctamente",
                    "maximaSalud creada", JOptionPane.PLAIN_MESSAGE);
            return true;
        } catch (SQLException | IOException e1) {
            e1.printStackTrace();
        }
        return false;
    }

    /***
     * Borrar la BBDD
     * @return
     */
    boolean borrarBBDD() {
        String sentenciaSql = "DROP DATABASE maximaSalud";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.execute();
            JOptionPane.showMessageDialog(null, "Base de datos eliminada correctamente",
                    "maximaSalud eliminada", JOptionPane.PLAIN_MESSAGE);
            return true;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
        return false;
    }

    /***
     * Leer el fichero con el script
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        //basedatos_java no tiene delimitador
        //StringBuilder es dinamica
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    /***
     * Desconectar de la BBDD
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            System.out.println("No se puede desconectar");
        }
    }

    /***
     * Insertar cliente
     */
    void insertarCliente(String nombre, String apellidos, String dni, LocalDate fechaNacimiento, LocalDate fechaAlta, String ciudad, String pais) {
        String sentenciaSql = "INSERT INTO clientes (nombre, apellidos, dni, fechanacimiento, fechaalta, ciudad, pais)" +
                "VALUES (?,?,?,?,?,?,?)";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, dni);
            sentencia.setDate(4, Date.valueOf(fechaNacimiento));
            sentencia.setDate(5, Date.valueOf(fechaAlta));
            sentencia.setString(6, ciudad);
            sentencia.setString(7, pais);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /***
     * Insertar tienda
     */
    void insertarTienda(String tienda, String email, String telefono, LocalDate fechaCreacion, String reputacion, String web, String ciudad) {
        String sentenciaSql = "INSERT INTO tiendas (tienda, email, telefono, fechacreacion, reputacion, web, ciudad) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, tienda);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setDate(4, Date.valueOf(fechaCreacion));
            sentencia.setString(5, reputacion);
            sentencia.setString(6, web);
            sentencia.setString(7, ciudad);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Insertar producto
     */
    void insertarProducto(String genero, String codigo, String tienda, String cliente,
                          float precio, LocalDate fechaEdicion, LocalDate fechaCaducidad, int cantidad) {
        String sentenciaSql = "INSERT INTO productos (genero, codigo, idtienda, idcliente, precio, fechaedicion, fechacaducidad, cantidad) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idcliente = Integer.valueOf(cliente.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, genero);
            sentencia.setString(2, codigo);
            sentencia.setInt(3, idtienda);
            sentencia.setInt(4, idcliente);
            sentencia.setFloat(5, precio);
            sentencia.setDate(6, Date.valueOf(fechaEdicion));
            sentencia.setDate(7, Date.valueOf(fechaCaducidad));
            sentencia.setInt(8, cantidad);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Insertar empleado
     */
    void insertarEmpleado(String nombreE, String apellidosE, String dniE, LocalDate fechaNacimientoE, String producto, String tienda, String paisE, Float sueldoE) {
        String sentenciaSql = "INSERT INTO empleados (nombreE, apellidosE, dniE, fechanacimientoE, idproducto, idtienda, paisE,sueldoE)" +
                "VALUES (?,?,?,?,?,?,?,?)";

        int idproducto = Integer.valueOf(producto.split(" ")[0]);
        int idtienda = Integer.valueOf(tienda.split(" ")[0]);

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreE);
            sentencia.setString(2, apellidosE);
            sentencia.setString(3, dniE);
            sentencia.setDate(4, Date.valueOf(fechaNacimientoE));
            sentencia.setInt(5, idproducto);
            sentencia.setInt(6, idtienda);
            sentencia.setString(7, paisE);
            sentencia.setFloat(8, sueldoE);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /***
     * Insertar compra
     */
    void insertarCompra(String codigoCompra, String cliente, String producto, String tienda, String empleado, int cantidadCompra, LocalDate fechaCompra) {
        String sentenciaSql = "INSERT INTO compras (codigoCompra, idcliente, idproducto, idtienda, idempleado, cantidadcompra, fechacompra) " +
                "VALUES (?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        int idproducto = Integer.valueOf(producto.split(" ")[0]);
        int idcliente = Integer.valueOf(cliente.split(" ")[0]);
        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idempleado = Integer.valueOf(empleado.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, codigoCompra);
            sentencia.setInt(2, idproducto);
            sentencia.setInt(3, idcliente);
            sentencia.setInt(4, idtienda);
            sentencia.setInt(5, idempleado);
            sentencia.setInt(6, cantidadCompra);
            sentencia.setDate(7, Date.valueOf(fechaCompra));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Modificar cliente
     */
    void modificarCliente(String nombre, String apellidos, String dni, LocalDate fechaNacimiento, LocalDate fechaAlta, String ciudad, String pais, int idcliente) {
        String sentenciaSql = "UPDATE clientes SET nombre=?,apellidos=?,dni=?,fechanacimiento=?,fechaalta=?,ciudad=?,pais=?" +
                "WHERE idcliente=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, dni);
            sentencia.setDate(4, Date.valueOf(fechaNacimiento));
            sentencia.setDate(5, Date.valueOf(fechaAlta));
            sentencia.setString(6, ciudad);
            sentencia.setString(7, pais);
            sentencia.setInt(8, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Modificar tienda
     */
    void modificarTienda(String tienda, String email, String telefono, LocalDate fechaCreacion, String reputacion, String web, String ciudad, int idtienda) {

        String sentenciaSql = "UPDATE tiendas SET tienda = ?, email = ?, telefono = ?, fechacreacion = ?, reputacion = ?, web = ?, ciudad = ?" +
                "WHERE idtienda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, tienda);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setDate(4, Date.valueOf(fechaCreacion));
            sentencia.setString(5, reputacion);
            sentencia.setString(6, web);
            sentencia.setString(7, ciudad);
            sentencia.setInt(8, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Modificar producto
     */
    void modificarProducto(String genero, String codigo, String tienda, String cliente,
                           float precio, LocalDate fechaEdicion, LocalDate fechaCaducidad, int cantidad, int idproducto) {

        String sentenciaSql = "UPDATE productos SET genero = ?, codigo = ?, idtienda = ?, " +
                "idcliente = ?, precio = ?, fechaedicion = ?, fechacaducidad=?, cantidad=? WHERE idproducto = ?";
        PreparedStatement sentencia = null;

        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idcliente = Integer.valueOf(cliente.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, genero);
            sentencia.setString(2, codigo);
            sentencia.setInt(3, idtienda);
            sentencia.setInt(4, idcliente);
            sentencia.setFloat(5, precio);
            sentencia.setDate(6, Date.valueOf(fechaEdicion));
            sentencia.setDate(7, Date.valueOf(fechaCaducidad));
            sentencia.setInt(8, cantidad);
            sentencia.setInt(9, idproducto);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Modificar empleado
     */
    void modificarEmpleado(String nombreE, String apellidosE, String dniE, LocalDate fechaNacimientoE, String producto, String tienda, String paisE, Float sueldoE, int idempleado) {
        String sentenciaSql = "UPDATE empleados SET nombreE=?,apellidosE=?,dniE=?,fechanacimientoE=?,idproducto=?,idtienda=?,paisE=?,sueldoE=?" +
                "WHERE idempleado=?";
        PreparedStatement sentencia = null;

        int idproducto = Integer.valueOf(producto.split(" ")[0]);
        int idtienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreE);
            sentencia.setString(2, apellidosE);
            sentencia.setString(3, dniE);
            sentencia.setDate(4, Date.valueOf(fechaNacimientoE));
            sentencia.setInt(5, idproducto);
            sentencia.setInt(6, idtienda);
            sentencia.setString(7, paisE);
            sentencia.setFloat(8, sueldoE);
            sentencia.setInt(9, idempleado);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Modificar compra
     */
    void modificarCompra(String codigoCompra, String producto, String cliente, String tienda, String empleado, int cantidadCompra, LocalDate fechaCompra, int idcompra) {
        String sentenciaSql = "UPDATE compras SET codigoCompra = ?, idproducto = ?, idcliente = ?, idtienda = ?,  " +
                "idempleado = ?, cantidadCompra = ?, fechaCompra = ? WHERE idcompra = ?";
        PreparedStatement sentencia = null;

        int idproducto = Integer.valueOf(producto.split(" ")[0]);
        int idcliente = Integer.valueOf(cliente.split(" ")[0]);
        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idempleado = Integer.valueOf(empleado.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, codigoCompra);
            sentencia.setInt(2, idproducto);
            sentencia.setInt(3, idcliente);
            sentencia.setInt(4, idtienda);
            sentencia.setInt(5, idempleado);
            sentencia.setInt(6, cantidadCompra);
            sentencia.setDate(7, Date.valueOf(fechaCompra));
            sentencia.setInt(8, idcompra);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Buscar producto
     */
    ResultSet buscarProducto(String codigoBuscar) {
        try {
            String consulta = "SELECT * FROM productos WHERE codigo = ?";
            PreparedStatement sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, codigoBuscar);
            return sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Buscar cliente
     */
    ResultSet buscarCliente(String dni) {
        try {
            String consulta = "SELECT * FROM clientes WHERE dni = ?";
            PreparedStatement sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, dni);
            return sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Buscar tienda
     */
    ResultSet buscarTienda(String nombreEdit) {
        try {
            String consulta = "SELECT * FROM tiendas WHERE tienda = ?";
            PreparedStatement sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombreEdit);
            return sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Buscar empleado
     */
    ResultSet buscarEmpleado(String dniE) {
        try {
            String consulta = "SELECT * FROM empleados WHERE dniE = ?";
            PreparedStatement sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, dniE);
            return sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Buscar compra
     */
    ResultSet buscarCompra(String codigoCompraBuscar) {
        try {
            String consulta = "SELECT * FROM compras WHERE codigoCompra = ?";
            PreparedStatement sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, codigoCompraBuscar);
            return sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Borrar tienda
     */
    void borrarTienda(int idtienda) {
        String sentenciaSql = "DELETE FROM tiendas WHERE idtienda=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Borrar cliente
     */
    void borrarCliente(int idcliente) {
        String sentenciaSql = "DELETE FROM clientes WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Borrar producto
     */
    void borrarProducto(int idproducto) {
        String sentenciaSql = "DELETE FROM productos WHERE idproducto = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idproducto);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Borrar empleado
     */
    void borrarEmpleado(int idempleado) {
        String sentenciaSql = "DELETE FROM empleados WHERE idempleado = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idempleado);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Borrar compra
     */
    void borrarCompra(int idcompra) {
        String sentenciaSql = "DELETE FROM compras WHERE idcompra = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcompra);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /***
     * Mostrar todas las tiendas
     */
    ResultSet consultarTienda() throws SQLException {
        String sentenciaSql = "SELECT concat(idtienda) AS 'ID', concat(tienda) AS 'Nombre tienda', " +
                "concat(email) AS 'email', concat(telefono) AS 'Teléfono'," +
                "concat(fechacreacion) AS 'Fecha de creacion', concat(reputacion) AS 'Reputacion'," +
                "concat(web) AS 'Web', concat(ciudad) AS 'Ciudad' FROM tiendas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /***
     * Mostrar todos los clientes
     */
    ResultSet consultarCliente() throws SQLException {
        String sentenciaSql = "SELECT concat(idcliente) AS 'ID', concat(nombre) AS 'Nombre', concat(apellidos) AS 'Apellidos', " +
                "concat(dni) AS 'DNI', concat(fechanacimiento) AS 'Fecha de nacimiento', " +
                "concat(fechaalta) AS 'Fecha de alta', concat(ciudad) AS 'Ciudad de origen', concat(pais) AS 'País de origen' FROM clientes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /***
     * Mostrar todos los productos
     */
    ResultSet consultarProductos() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idproducto) AS 'ID', concat(b.genero) AS 'Género', concat(b.codigo) AS 'Codigo', " +
                "concat(e.idtienda, ' - ', e.tienda) AS 'Tienda', " +
                "concat(a.idcliente, ' - ', a.apellidos, ', ', a.nombre) AS 'Cliente', " +
                "concat(b.precio) AS 'Precio', concat(b.fechaedicion) AS 'Fecha de edición', " +
                "concat(b.fechacaducidad) AS 'Fecha de caducidad', concat(b.cantidad) AS 'Cantidad' FROM productos AS b " +
                "INNER JOIN tiendas AS e ON e.idtienda = b.idtienda INNER JOIN " +
                "clientes AS a ON a.idcliente = b.idcliente";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /***
     * Mostrar todos los empleados
     */
    ResultSet consultarEmpleado() throws SQLException {
        String sentenciaSql = "SELECT concat(idempleado) AS 'ID', concat(nombreE) AS 'Nombre', concat(apellidosE) AS 'Apellidos', " +
                "concat(dniE) AS 'DNI', concat(fechanacimientoE) AS 'Fecha de nacimiento', " +
                "concat(p.idproducto, ' - ', p.genero) AS 'Producto', concat(t.idtienda, ' - ', t.tienda) AS 'Tienda', " +
                "concat(paisE) AS 'País de origen', concat(sueldoE) AS 'Sueldo' FROM empleados AS e " +
                "INNER JOIN tiendas AS t ON t.idtienda = e.idtienda " +
                "INNER JOIN productos AS p ON p.idproducto = e.idproducto";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /***
     * Mostrar todas las compras
     */
    ResultSet consultarCompras() throws SQLException {
        String sentenciaSql = "SELECT concat(p.idcompra) AS 'ID', concat(p.codigoCompra) AS 'Codigo', " +
                "concat(b.idproducto, ' - ', b.genero) AS 'Producto', " +
                "concat(a.idcliente, ' - ', a.apellidos, ', ', a.nombre) AS 'Cliente', " +
                "concat(t.idtienda, ' - ', t.tienda) AS 'Tienda', " +
                "concat(e.idempleado, ' - ', e.apellidosE, ', ', e.nombreE) AS 'Empleado', " +
                "concat(p.cantidadCompra) AS 'Compra',  concat(p.fechacompra) AS 'Fecha de compra' " +
                "FROM compras AS p " +
                "INNER JOIN tiendas AS t ON t.idtienda = p.idtienda INNER JOIN " +
                "clientes AS a ON a.idcliente = p.idcliente " +
                "INNER JOIN productos AS b ON b.idproducto = p.idproducto INNER JOIN " +
                "empleados AS e ON e.idempleado = p.idempleado";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /***
     * Usar datos del cuadro de diálogo
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /***
     * Establecer los datos del cuadro de diálogo
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /***
     * Comprobaciones llamando a funciones de sql
     */
    public boolean productoCodigoYaExiste(String codigo) {
        String consulta = "SELECT existeCodigo(?)";
        PreparedStatement function;
        boolean codigoExists = false;
        try {
            function = conexion.prepareStatement(consulta);
            function.setString(1, codigo);
            ResultSet rs = function.executeQuery();
            rs.next();
            codigoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoExists;
    }

    /***
     * Comprobaciones llamando a funciones de sql
     */
    public boolean tiendaNombreYaExiste(String nombre) {
        String tiendaNameConsult = "SELECT existeNombreTienda(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(tiendaNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();
            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /***
     * Comprobaciones llamando a funciones de sql
     */
    public boolean clienteNombreYaExiste(String nombre, String apellidos) {
        String completeName = apellidos + ", " + nombre;
        String authorNameConsult = "SELECT existeNombreCliente(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();
            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /***
     * Comprobaciones llamando a funciones de sql
     */
    public boolean empleadoNombreYaExiste(String nombreE, String apellidosE) {
        String completeNameE = apellidosE + ", " + nombreE;
        String authorNameConsultE = "SELECT existeNombreEmpleado(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsultE);
            function.setString(1, completeNameE);
            ResultSet rs = function.executeQuery();
            rs.next();
            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /***
     * Comprobaciones llamando a funciones de sql
     */
    public boolean compraCodigoYaExiste(String codigoCompra) {
        String consulta = "SELECT existeCodigoCompra(?)";
        PreparedStatement function;
        boolean codigoExists = false;
        try {
            function = conexion.prepareStatement(consulta);
            function.setString(1, codigoCompra);
            ResultSet rs = function.executeQuery();
            rs.next();
            codigoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoExists;
    }
}