package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Eliminar extends JFrame {
	//Constructor de la clase eliminar
	public Eliminar() {
		this.setTitle("Eliminar");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		elim();
	}
	
	public void elim() {
		//colocar los componentes en la ventana
		JLabel labeljtf = new JLabel("�Donde desea eliminar datos?");
		labeljtf.setBounds(240,10,210,30);
		labeljtf.setFont(new Font("Arial", 1, 12));
		this.add(labeljtf);
		
		String elementos[] = {"","Empleados","Tiendas","Usuarios"};
		
		JComboBox jcb = new JComboBox(elementos);
		jcb.setBounds(220,60,210,30);
		jcb.setFont(new Font("Arial", 0, 12));
		this.add(jcb);
		
		JButton boton = new JButton("OK");
		boton.setBounds(220,120,100,30);
		boton.setFont(new Font("Arial", 0, 12));
		this.add(boton);
		
		JButton botonCanc = new JButton("Cancelar");
		botonCanc.setBounds(330,120,100,30);
		botonCanc.setFont(new Font("Arial", 0, 12));
		this.add(botonCanc);
		
		//escuchador
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jcb.getSelectedItem()=="Empleados") {
					EliminarEmp elimE = new EliminarEmp();
					elimE.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Tiendas") {
					EliminarTi elimT = new EliminarTi();
					elimT.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Usuarios") {
					EliminarUs elimU = new EliminarUs();
					elimU.setVisible(true);
					dispose();
				}
			}
		});
		
		botonCanc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}

	public static void main(String[] args) {
		//Se crea un nuevo eliminar
		Eliminar e = new Eliminar();
		e.setVisible(true);
	}
}
