package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Inicio extends JFrame {
	//maximaSalud maximaSalud;
	public Inicio() {
		this.setTitle("Inicio");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(true);
		this.setLayout(null);
		iSesion();
	}
	
	public void iSesion() {
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(60,160,150,150);
		ImageIcon i = new ImageIcon(getClass().getResource("perfil.JPG"));
		ImageIcon img = new ImageIcon(i.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_SMOOTH));
		lblNewLabel.setIcon(img);
		this.add(lblNewLabel);
		
		JButton botonA = new JButton("Administrador");
		botonA.setBounds(60,330,150,30);
		botonA.setFont(new Font("Arial", 0, 12));
		this.add(botonA);
		
		JLabel lblNewLabel1 = new JLabel("");
		lblNewLabel1.setBounds(260,160,150,150);
		ImageIcon i1 = new ImageIcon(getClass().getResource("perfil.JPG"));
		ImageIcon img1 = new ImageIcon(i1.getImage().getScaledInstance(lblNewLabel1.getWidth(), lblNewLabel1.getHeight(), Image.SCALE_SMOOTH));
		lblNewLabel1.setIcon(img1);
		this.add(lblNewLabel1);
		
		JButton botonE = new JButton("Edicion");
		botonE.setBounds(260,330,150,30);
		botonE.setFont(new Font("Arial", 0, 12));
		this.add(botonE);
		
		JLabel lblNewLabel2 = new JLabel("");
		lblNewLabel2.setBounds(460,160,150,150);
		ImageIcon i2 = new ImageIcon(getClass().getResource("perfil.JPG"));
		ImageIcon img2 = new ImageIcon(i2.getImage().getScaledInstance(lblNewLabel2.getWidth(), lblNewLabel2.getHeight(), Image.SCALE_SMOOTH));
		lblNewLabel2.setIcon(img2);
		this.add(lblNewLabel2);
		
		JButton botonC = new JButton("Consulta");
		botonC.setBounds(460,330,150,30);
		botonC.setFont(new Font("Arial", 0, 12));
		this.add(botonC);
		
		//escuchador
		botonA.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//introducir un valor en un cuadro de dialogo
				String codigo = JOptionPane.showInputDialog("Introduce la clave de administrador: ");
				String pwAdmin = "Admin2022";
				
				if (codigo.equals(pwAdmin)) {
					JOptionPane.showMessageDialog(null, "La clave de administrador es correcta", "Informacion", JOptionPane.INFORMATION_MESSAGE);
					InicioSesion is = new InicioSesion();
					is.setVisible(true);
					dispose();
				} else {
					JOptionPane.showMessageDialog(null, "La clave de administrador es incorrecta", "Informacion", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
		botonE.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//introducir un valor en un cuadro de dialogo
				String codigo = JOptionPane.showInputDialog(null, "Introduce la clave de edicion: ", "Edicion2022", JOptionPane.QUESTION_MESSAGE);
				String pwEdi = "Edicion2022";
				if (codigo.equals(pwEdi)) {
					JOptionPane.showMessageDialog(null, "La clave de edicion es correcta", "Informacion", JOptionPane.INFORMATION_MESSAGE);
					InicioE ie = new InicioE();
					ie.setVisible(true);
					dispose();
				} else {
					JOptionPane.showMessageDialog(null, "La clave de edicion es incorrecta", "Informacion", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
		botonC.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				InicioC ic = new InicioC();
				ic.setVisible(true);
				dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo cliente
		Inicio i = new Inicio();
		i.setVisible(true);
	}
}