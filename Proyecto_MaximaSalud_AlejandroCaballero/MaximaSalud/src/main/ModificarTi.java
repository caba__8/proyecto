package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ModificarTi extends JFrame {
	//Constructor de la clase modificar tienda
	public ModificarTi() {
		this.setTitle("Modificar tienda");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		modifT();
	}
	
	public void modifT() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				//colocar los componentes en la ventana
				JLabel labelnomA = new JLabel("Nombre de la tienda que quieres actualizar:");
				labelnomA.setBounds(200,10,245,30);
				labelnomA.setFont(new Font("Arial", 1, 12));
				this.add(labelnomA);
				
				JTextField jtfnomA = new JTextField();
				jtfnomA.setBounds(150,50,360,30);
				jtfnomA.setFont(new Font("Arial", 0, 12));
				this.add(jtfnomA);
				
				JLabel labeldatos = new JLabel("Introduce los nuevos datos: ");
				labeldatos.setBounds(240,90,200,30);
				labeldatos.setFont(new Font("Arial", 1, 12));
				this.add(labeldatos);
				
				JLabel labelid = new JLabel("Id:");
				labelid.setBounds(10,130,200,30);
				labelid.setFont(new Font("Arial", 1, 12));
				this.add(labelid);
				
				JTextField jtf = new JTextField();
				jtf.setBounds(150,130,360,30);
				jtf.setFont(new Font("Arial", 0, 12));
				this.add(jtf);
				
				JLabel labelnom = new JLabel("Nombre:");
				labelnom.setBounds(10,180,200,30);
				labelnom.setFont(new Font("Arial", 1, 12));
				this.add(labelnom);
				
				JTextField jtfnom = new JTextField();
				jtfnom.setBounds(150,180,360,30);
				jtfnom.setFont(new Font("Arial", 0, 12));
				this.add(jtfnom);
				
				JLabel labelem = new JLabel("Email:");
				labelem.setBounds(10,230,200,30);
				labelem.setFont(new Font("Arial", 1, 12));
				this.add(labelem);
				
				JTextField jtfem = new JTextField();
				jtfem.setBounds(150,230,360,30);
				jtfem.setFont(new Font("Arial", 0, 12));
				this.add(jtfem);
				
				JLabel labeltfn = new JLabel("Telefono:");
				labeltfn.setBounds(10,280,200,30);
				labeltfn.setFont(new Font("Arial", 1, 12));
				this.add(labeltfn);
				
				JTextField jtftfn = new JTextField();
				jtftfn.setBounds(150,280,360,30);
				jtftfn.setFont(new Font("Arial", 0, 12));
				this.add(jtftfn);
				
				JLabel labeldate = new JLabel("Fecha creacion:");
				labeldate.setBounds(10,330,200,30);
				labeldate.setFont(new Font("Arial", 1, 12));
				this.add(labeldate);
				
				JTextField jtfdate = new JTextField();
				jtfdate.setBounds(150,330,350,30);
				jtfdate.setFont(new Font("Arial", 0, 12));
				this.add(jtfdate);
				
				JLabel labelrepu = new JLabel("Reputacion:");
				labelrepu.setBounds(10,380,200,30);
				labelrepu.setFont(new Font("Arial", 1, 12));
				this.add(labelrepu);
				
				JTextField jtfrepu = new JTextField();
				jtfrepu.setBounds(150,380,350,30);
				jtfrepu.setFont(new Font("Arial", 0, 12));
				this.add(jtfrepu);				
				
				JLabel labeldir = new JLabel("Web:");
				labeldir.setBounds(10,430,200,30);
				labeldir.setFont(new Font("Arial", 1, 12));
				this.add(labeldir);
				
				JTextField jtfdir = new JTextField();
				jtfdir.setBounds(150,430,350,30);
				jtfdir.setFont(new Font("Arial", 0, 12));
				this.add(jtfdir);
				
				JLabel labelciudad = new JLabel("Ciudad:");
				labelciudad.setBounds(10,480,200,30);
				labelciudad.setFont(new Font("Arial", 1, 12));
				this.add(labelciudad);
				
				JTextField jtfciudad = new JTextField();
				jtfciudad.setBounds(150,480,350,30);
				jtfciudad.setFont(new Font("Arial", 0, 12));
				this.add(jtfciudad);
				
				JButton boton = new JButton("OK");
				boton.setBounds(530,130,115,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(530,180,115,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
				
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					
					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						String query="UPDATE tiendas SET idtienda="+Integer.parseInt(jtf.getText()) +",tienda='"+jtfnom.getText() +"', fechacreacion='" + jtfdate.getText() +"', reputacion='" + jtfrepu.getText()+ "', email='" + jtfem.getText() + "', telefono='" + jtftfn.getText() + "',web='"+jtfdir.getText() + "', ciudad='" + jtfciudad.getText() + "' WHERE tienda='"+jtfnomA.getText()+"';";
						
						try {
							sentencia= this.conexion.prepareStatement(query);
							sentencia.executeUpdate();	
							JOptionPane.showMessageDialog(null, "La operacion se ha realizado con exito", "Informacion", JOptionPane.INFORMATION_MESSAGE);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				});
				
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);
					}
				});
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo modificar tienda
		ModificarTi modifTi = new ModificarTi();
		modifTi.setVisible(true);
	}
}