package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *Clase Combo. Representa a la aplicaci�n combo del ejercicio.
 */
public class Consulta extends JFrame {
	//Constructor de la clase combo
	public Consulta() {
		this.setTitle("Consultas");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		comboBox();
	}
	
	public static class Conexion {
		public static Connection getMySQLConexion() {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				//conectamos con la base de datos
				Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
				return conexion;
			} catch (ClassNotFoundException | SQLException ex ) {
				Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE,null,ex);
			}
			return null;
		}
	}
	
	public void comboBox() {
		//meter varios: JLabel de datos(los 4 jaspers que tenemos ya), graficos, lo que haya en pdf
		
		//colocar los componentes en la ventana
		JLabel labeljtf = new JLabel("�Que desea consultar?");
		labeljtf.setBounds(260,10,280,30);
		labeljtf.setFont(new Font("Arial", 1, 12));
		this.add(labeljtf);
		
		String elementos[] = {"","Empleados","Productos","Tiendas", "Productos ordenados"};
		
		JComboBox jcb = new JComboBox(elementos);
		jcb.setBounds(220,60,210,30);
		jcb.setFont(new Font("Arial", 0, 12));
		this.add(jcb);
		
		JButton boton = new JButton("OK");
		boton.setBounds(220,120,100,30);
		boton.setFont(new Font("Arial", 0, 12));
		this.add(boton);
		
		JButton botonCanc = new JButton("Cancelar");
		botonCanc.setBounds(330,120,100,30);
		botonCanc.setFont(new Font("Arial", 0, 12));
		this.add(botonCanc);
		
		//Accion
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (jcb.getSelectedItem()=="Productos") {
			        JasperPrint informeLleno= ReportGeneratorProductos.generarInformeProductos("Ibuprofeno");
		    	    JasperViewer viewer = new JasperViewer(informeLleno, false);
		    	    viewer.setVisible(true);
		    	    try {
						JasperExportManager.exportReportToPdfFile(informeLleno, "productos.pdf");
					} catch (JRException ex) {
						ex.printStackTrace();
					}
		    	}
				
				if (jcb.getSelectedItem()=="Productos ordenados") {
			        JasperPrint informeLleno= ReportGeneratorProductosCodigo.generarInformeProductosCodigo("Dalsy");
		    	    JasperViewer viewer = new JasperViewer(informeLleno, false);
		    	    viewer.setVisible(true);
		    	    try {
						JasperExportManager.exportReportToPdfFile(informeLleno, "productosOrdenados.pdf");
					} catch (JRException ex) {
						ex.printStackTrace();
					}
		    	}
				
				if (jcb.getSelectedItem()=="Empleados") {
					JasperPrint informeLleno= ReportGeneratorEmpleados.generarInformeEmpleados("Jorge");
		    	    JasperViewer viewer = new JasperViewer(informeLleno, false);
		    	    viewer.setVisible(true);
		    	    try {
						JasperExportManager.exportReportToPdfFile(informeLleno, "empleados.pdf");
					} catch (JRException ex) {
						ex.printStackTrace();
					}
		    	}
					
				if (jcb.getSelectedItem()=="Tiendas") {
					JasperPrint informeLleno= ReportGeneratorTiendas.generarInformeTiendas("Teva");
		    	    JasperViewer viewer = new JasperViewer(informeLleno, false);
		    	    viewer.setVisible(true);
		    	    try {
						JasperExportManager.exportReportToPdfFile(informeLleno, "tiendas.pdf");
					} catch (JRException ex) {
						ex.printStackTrace();
					}
		    	}
			}
		});
		
		botonCanc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}
	
	private void EnviarError() {
        System.out.println("Error");
    }
	
	public static class ReportGeneratorProductos {
		public static final String I_P = "ProyectoP.jasper";
		//indicamos el nombre del fichero jasper
	
		//creamos el metodo pasar por parametros la calificacion
		public static JasperPrint generarInformeProductos(String producto) {
			// pasamos el HasMap que contendra el parametro de tipo string
			// y el objeto como valor 
			
			HashMap<String,	Object> parametros = new HashMap<>();
			//en los parametros pongo los valores como se llama 
			//en el informe y el valor que paso
			parametros.put("producto", producto);
			
			try {
				JasperPrint informeLleno = JasperFillManager.fillReport
						(I_P, parametros, Conexion.getMySQLConexion());
				return informeLleno;
			} catch (JRException e) {
				e.printStackTrace();
			}
			return null;	
		}
	}
	
	public static class ReportGeneratorProductosCodigo {
		public static final String I_P = "ProductosCodigo.jasper";
		//indicamos el nombre del fichero jasper
	
		//creamos el metodo pasar por parametros la calificacion
		public static JasperPrint generarInformeProductosCodigo(String productoCod) {
			// pasamos el HasMap que contendra el parametro de tipo string
			// y el objeto como valor 
			
			HashMap<String,	Object> parametros = new HashMap<>();
			//en los parametros pongo los valores como se llama 
			//en el informe y el valor que paso
			parametros.put("producto", productoCod);
			
			try {
				JasperPrint informeLleno = JasperFillManager.fillReport
						(I_P, parametros, Conexion.getMySQLConexion());
				return informeLleno;
			} catch (JRException e) {
				e.printStackTrace();
			}
			return null;	
		}
	}
	
	public static class ReportGeneratorEmpleados {
		public static final String I_P = "ProyectoE.jasper";
		//indicamos el nombre del fichero jasper
	
		//creamos el metodo pasar por parametros la calificacion
		public static JasperPrint generarInformeEmpleados(String empleado) {
			// pasamos el HasMap que contendra el parametro de tipo string
			// y el objeto como valor 
			
			HashMap<String,	Object> parametros = new HashMap<>();
			//en los parametros pongo los valores como se llama 
			//en el informe y el valor que paso
			parametros.put("empleado", empleado);
			
			try {
				JasperPrint informeLleno = JasperFillManager.fillReport
						(I_P, parametros, Conexion.getMySQLConexion());
				return informeLleno;
			} catch (JRException e) {
				e.printStackTrace();
			}
			return null;	
		}
	}
	
	public static class ReportGeneratorTiendas {
		public static final String I_P = "ProyectoT.jasper";
		//indicamos el nombre del fichero jasper
		
		//creamos el metodo pasar por parametros la calificacion
		public static JasperPrint generarInformeTiendas(String tienda) {
			// pasamos el HasMap que contendra el parametro de tipo string
			// y el objeto como valor 
			
			HashMap<String,	Object> parametros = new HashMap<>();
			//en los parametros pongo los valores como se llama 
			//en el informe y el valor que paso
			parametros.put("tienda", tienda);
			
			try {
				JasperPrint informeLleno = JasperFillManager.fillReport
						(I_P, parametros, Conexion.getMySQLConexion());
				return informeLleno;
			} catch (JRException e) {
				e.printStackTrace();
			}
			return null;	
		}
	}
	
	public static void main(String[] args) throws IOException, SQLException {
		//Se crea un nuevo combo
		Consulta c = new Consulta();
		c.setVisible(true);
	}
}