package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class InicioC extends JFrame {
	
	LibreriaCifrado cifrado;

	public InicioC() {
		this.setTitle("Inicio sesion consulta");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		iSesion();
	}
	
	public void iSesion() {
		cifrado = new LibreriaCifrado();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				JLabel labeljtf = new JLabel("Usuario:");
				labeljtf.setBounds(100,10,120,30);
				labeljtf.setFont(new Font("Arial", 1, 12));
				this.add(labeljtf);
				
				JTextField jtf = new JTextField();
				jtf.setBounds(200,10,320,30);
				jtf.setFont(new Font("Arial", 0, 12));
				this.add(jtf);
				
				JLabel labeljtfR = new JLabel("Contrasena:");
				labeljtfR.setBounds(100,60,320,30);
				labeljtfR.setFont(new Font("Arial", 1, 12));
				this.add(labeljtfR);
				
				JPasswordField jpf = new JPasswordField();
				jpf.setBounds(200,60,320,30);
				jpf.setFont(new Font("Arial", 0, 12));
				this.add(jpf);
				
        		JButton boton = new JButton("OK");
				boton.setBounds(200,120,100,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonR = new JButton("Registrarse");
				botonR.setBounds(310,120,100,30);
				botonR.setFont(new Font("Arial", 0, 12));
				this.add(botonR);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(420,120,100,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
				
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					ResultSet resultado;
					
					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						String query = "SELECT * FROM USUARIO WHERE NOMBRE_USER='"+ cifrado.cifrar(jtf.getText()) +"'AND PW_USER='"+ cifrado.hashear(jpf.getText()) +"';";
						int resultado=0;
						try {
							sentencia=this.conexion.prepareStatement(query);
							ResultSet comprobacion = sentencia.executeQuery();
							String usuarioCorrecto = null;
						    String passwordCorrecta = null;
						    
							if (comprobacion.next()) {
								usuarioCorrecto = comprobacion.getString(1);
						        passwordCorrecta = comprobacion.getString(2);
							}
							if (usuarioCorrecto!=null && cifrado.hashear(jpf.getText())!=null && cifrado.hashear(jpf.getText()).equals(passwordCorrecta)) {
					        	JOptionPane.showMessageDialog(null, "Inicio correcto", "Informacion", JOptionPane.INFORMATION_MESSAGE);
					        	Consulta c = new Consulta();
								c.setVisible(true);
								dispose();
					        } else {
								JOptionPane.showMessageDialog(null, "Inicio incorrecto", "Informacion", JOptionPane.INFORMATION_MESSAGE);
							}
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				});
				
				botonR.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						RegistroCon rc = new RegistroCon();
						rc.setVisible(true);
						dispose();
					}
				});
				
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);
					}
				});
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo cliente
		InicioC ic = new InicioC();
		ic.setVisible(true);
	}
}
