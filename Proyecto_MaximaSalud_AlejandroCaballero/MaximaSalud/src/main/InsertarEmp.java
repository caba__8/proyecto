package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class InsertarEmp extends JFrame {

	//Constructor de la clase insertar empleado
	public InsertarEmp() {
		this.setTitle("Insertar empleado");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		insertE();
	}
	
	public void insertE() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				//colocar los componentes en la ventana
				JLabel labelid = new JLabel("Id:");
				labelid.setBounds(10,10,200,30);
				labelid.setFont(new Font("Arial", 1, 12));
				this.add(labelid);
		
				JTextField jtf = new JTextField();
				jtf.setBounds(150,10,350,30);
				jtf.setFont(new Font("Arial", 0, 12));
				this.add(jtf);
				
				JLabel labelnom = new JLabel("Nombre:");
				labelnom.setBounds(10,60,200,30);
				labelnom.setFont(new Font("Arial", 1, 12));
				this.add(labelnom);
				
				JTextField jtfnom = new JTextField();
				jtfnom.setBounds(150,60,350,30);
				jtfnom.setFont(new Font("Arial", 0, 12));
				this.add(jtfnom);
				
				JLabel labelap = new JLabel("Apellido:");
				labelap.setBounds(10,110,200,30);
				labelap.setFont(new Font("Arial", 1, 12));
				this.add(labelap);
				
				JTextField jtfap = new JTextField();
				jtfap.setBounds(150,110,350,30);
				jtfap.setFont(new Font("Arial", 0, 12));
				this.add(jtfap);
				
				JLabel labeldni = new JLabel("DNI:");
				labeldni.setBounds(10,160,200,30);
				labeldni.setFont(new Font("Arial", 1, 12));
				this.add(labeldni);
				
				JTextField jtfdni = new JTextField();
				jtfdni.setBounds(150,160,350,30);
				jtfdni.setFont(new Font("Arial", 0, 12));
				this.add(jtfdni);
				
				JLabel labeldate = new JLabel("Fecha nacimiento:");
				labeldate.setBounds(10,210,200,30);
				labeldate.setFont(new Font("Arial", 1, 12));
				this.add(labeldate);
				
				JTextField jtfdate = new JTextField();
				jtfdate.setBounds(150,210,350,30);
				jtfdate.setFont(new Font("Arial", 0, 12));
				this.add(jtfdate);
		
				JLabel labelprod = new JLabel("Producto:");
				labelprod.setBounds(10,260,200,30);
				labelprod.setFont(new Font("Arial", 1, 12));
				this.add(labelprod);
				
				JTextField jtfprod = new JTextField();
				jtfprod.setBounds(150,260,350,30);
				jtfprod.setFont(new Font("Arial", 0, 12));
				this.add(jtfprod);
				
				JLabel labeltienda = new JLabel("Tienda:");
				labeltienda.setBounds(10,310,200,30);
				labeltienda.setFont(new Font("Arial", 1, 12));
				this.add(labeltienda);
				
				JTextField jtftienda = new JTextField();
				jtftienda.setBounds(150,310,350,30);
				jtftienda.setFont(new Font("Arial", 0, 12));
				this.add(jtftienda);
		
				JLabel labelpais = new JLabel("Pais:");
				labelpais.setBounds(10,360,200,30);
				labelpais.setFont(new Font("Arial", 1, 12));
				this.add(labelpais);
				
				JTextField jtfpais = new JTextField();
				jtfpais.setBounds(150,360,350,30);
				jtfpais.setFont(new Font("Arial", 0, 12));
				this.add(jtfpais);
				
				JLabel labelsueldo = new JLabel("Sueldo:");
				labelsueldo.setBounds(10,410,200,30);
				labelsueldo.setFont(new Font("Arial", 1, 12));
				this.add(labelsueldo);
				
				JTextField jtfsueldo = new JTextField();
				jtfsueldo.setBounds(150,410,350,30);
				jtfsueldo.setFont(new Font("Arial", 0, 12));
				this.add(jtfsueldo);
				
				JButton boton = new JButton("OK");
				boton.setBounds(520,10,115,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(520,60,115,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
		
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					
					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						String query = "INSERT INTO empleados VALUES (" + Integer.parseInt(jtf.getText())+", '" + jtfnom.getText()+"', '" + jtfap.getText()+"', '" + jtfdni.getText() +"', '" + jtfdate.getText()+"', '" + jtfprod.getText()+"', '" + jtftienda.getText()+"', '" + jtfpais.getText()+"', '" + Float.parseFloat(jtfsueldo.getText())+"');";
				
						try {
							sentencia=this.conexion.prepareStatement(query);
							sentencia.executeUpdate();
							JOptionPane.showMessageDialog(null, "La operacion se ha realizado con exito", "Informacion", JOptionPane.INFORMATION_MESSAGE);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				});
		
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);
					}
				});
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo insertar
		InsertarEmp insertEmp = new InsertarEmp();
		insertEmp.setVisible(true);
	}
}