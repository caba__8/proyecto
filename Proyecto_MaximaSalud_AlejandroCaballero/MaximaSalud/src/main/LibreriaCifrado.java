package main;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class LibreriaCifrado {

	public String cifrar(String texto) {
		try {
			// Generamos una clave de 128 bits adecuada para AES
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
	        keyGenerator.init(128);
	        Key key = keyGenerator.generateKey();
	        
	        // Alternativamente, una clave que queramos que tenga al menos 16 bytes y nos quedamos con los bytes 0 a 15
            key = new SecretKeySpec("una clave de 16 bytes".getBytes(), 0, 16, "AES");
            
		    System.out.println();
		    
		    // Se obtiene un cifrador AES
		    Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
			
		    // Se inicializa para encriptacion y se encripta el texto
		    aes.init(Cipher.ENCRYPT_MODE, key);
		    byte[] encriptado = aes.doFinal(texto.getBytes());
		    
		    // Se devuelve el mensaje cifrado, codificado en Base64 como String
		    return Base64.getEncoder().encodeToString(encriptado);
		    //String base = Base64.getEncoder().encodeToString(encriptado);
		    //return base;
		    
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String descifrar(String texto) {
		try {
			// Generamos una clave de 128 bits
	        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
	        keyGenerator.init(128);
	        Key key = keyGenerator.generateKey();
	        
	        // Alternativamente, una clave que queramos que tenga al menos 16 bytes y nos quedamos con los bytes 0 a 15
	        key = new SecretKeySpec("una clave de 16 bytes".getBytes(), 0, 16, "AES");
	        
		    System.out.println();
	        
	        // Se obtiene un cifrador AES
		    Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
		    
		    // Se inicializa para encriptacion y se encripta el texto
		    //aes.init(Cipher.ENCRYPT_MODE, key);
		    //byte[] encriptado = aes.doFinal(texto.getBytes());
			//String base = Base64.getEncoder().encodeToString(encriptado);
			
			// Se iniciliza el cifrador para desencriptar, con la misma clave y se desencripta
	        aes.init(Cipher.DECRYPT_MODE, key);
	        byte[] desencriptado = aes.doFinal(Base64.getDecoder().decode(texto.getBytes()));
	        
	        // Se devuelve el mensaje descifrado como String
	        return new String(desencriptado);
	        
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String hashear(String texto) {
		try {
			//Se crea el objeto MessageDigest
	        MessageDigest frase = null;
	        //SHA-512
			frase = MessageDigest.getInstance("SHA-512");
	        frase.update(texto.getBytes());
	        //Se realiza el Hashing
	        byte[] mensaje = frase.digest();
	        //Se muestra por pantalla
	        return new String(Hex.encodeHex(mensaje));
			//return mensaje; //guardar mensaje en bd
        } catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean verificar(String textoHash, String texto) {
		String textoHasheado = hashear(texto); //para ver si el hash texto es igual al de la base de datos
		return textoHasheado.equals(textoHash);
	}
	
}
