package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class ModificarUs extends JFrame {
	
	LibreriaCifrado cifrado;
	
	//Constructor de la clase modificar usuario
	public ModificarUs() {
		this.setTitle("Modificar usuario");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setResizable(true);
		modifU();
	}
	
	public void modifU() {
		cifrado = new LibreriaCifrado();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				//colocar los componentes en la ventana
				JLabel nombreA = new JLabel("Nombre del usuario que quieres actualizar:");
				nombreA.setBounds(200,10,245,30);
				nombreA.setFont(new Font("Arial", 1, 12));
				this.add(nombreA);
				
				JTextField jtfnomA = new JTextField();
				jtfnomA.setBounds(150,50,360,30);
				jtfnomA.setFont(new Font("Arial", 0, 12));
				this.add(jtfnomA);
				
				JLabel labeldatos = new JLabel("Introduce los nuevos datos: ");
				labeldatos.setBounds(240,90,200,30);
				labeldatos.setFont(new Font("Arial", 1, 12));
				this.add(labeldatos);
				
				JLabel labelnom = new JLabel("Nombre:");
				labelnom.setBounds(10,130,200,30);
				labelnom.setFont(new Font("Arial", 1, 12));
				this.add(labelnom);
				
				JTextField jtfnom = new JTextField();
				jtfnom.setBounds(150,130,360,30);
				jtfnom.setFont(new Font("Arial", 0, 12));
				this.add(jtfnom);
				
				JLabel labelpw = new JLabel("Contrasena:");
				labelpw.setBounds(10,180,200,30);
				labelpw.setFont(new Font("Arial", 1, 12));
				this.add(labelpw);
				
				JPasswordField jtfpw = new JPasswordField();
				jtfpw.setBounds(150,180,360,30);
				jtfpw.setFont(new Font("Arial", 0, 12));
				this.add(jtfpw);
				
				JLabel labelemail = new JLabel("Email:");
				labelemail.setBounds(10,230,200,30);
				labelemail.setFont(new Font("Arial", 1, 12));
				this.add(labelemail);
				
				JTextField jtfem = new JTextField();
				jtfem.setBounds(150,230,360,30);
				jtfem.setFont(new Font("Arial", 0, 12));
				this.add(jtfem);
				
				JLabel labeltel = new JLabel("Telefono:");
				labeltel.setBounds(10,280,200,30);
				labeltel.setFont(new Font("Arial", 1, 12));
				this.add(labeltel);
				
				JTextField jtftfn = new JTextField();
				jtftfn.setBounds(150,280,360,30);
				jtftfn.setFont(new Font("Arial", 0, 12));
				this.add(jtftfn);
				
				JButton boton = new JButton("OK");
				boton.setBounds(200,330,100,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(350,330,100,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
		
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					
					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						String query="UPDATE USUARIO SET NOMBRE_USER='"+cifrado.cifrar(jtfnom.getText()) +"', PW_USER='"+ cifrado.hashear(jtfpw.getText())+"', EMAIL_USER='" + cifrado.cifrar(jtfem.getText()) +"', TELEFONO_USER='" + cifrado.cifrar(jtftfn.getText()) +"' WHERE NOMBRE_USER='"+cifrado.cifrar(jtfnomA.getText())+"';";
						
						try {
							sentencia= this.conexion.prepareStatement(query);
							sentencia.executeUpdate();	
							JOptionPane.showMessageDialog(null, "La operacion se ha realizado con exito", "Informacion", JOptionPane.INFORMATION_MESSAGE);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				});
				
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);
					}
				});
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo modificar usuario
		ModificarUs modifUser = new ModificarUs();
		modifUser.setVisible(true);
	}
}