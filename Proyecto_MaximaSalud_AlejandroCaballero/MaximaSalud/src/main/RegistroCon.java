package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RegistroCon extends JFrame {
	
	LibreriaCifrado cifrado;
	
	public RegistroCon() {
		this.setTitle("Inicio consulta");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		rCon();
	}
	
	public void rCon() {
		cifrado = new LibreriaCifrado();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				JLabel labeljtf = new JLabel("Usuario:");
				labeljtf.setBounds(100,10,120,30);
				labeljtf.setFont(new Font("Arial", 1, 12));
				this.add(labeljtf);
				
				JTextField jtf = new JTextField();
				jtf.setBounds(230,10,300,30);
				jtf.setFont(new Font("Arial", 0, 12));
				this.add(jtf);
				
				JLabel labelPw = new JLabel("Contrasena:");
				labelPw.setBounds(100,60,120,30);
				labelPw.setFont(new Font("Arial", 1, 12));
				this.add(labelPw);
				
				JPasswordField jpf = new JPasswordField();
				jpf.setBounds(230,60,300,30);
				jpf.setFont(new Font("Arial", 0, 12));
				this.add(jpf);
				
				JLabel labeljtfEm = new JLabel("Email:");
				labeljtfEm.setBounds(100,110,120,30);
				labeljtfEm.setFont(new Font("Arial", 1, 12));
				this.add(labeljtfEm);
				
				JTextField jtfEm = new JTextField();
				jtfEm.setBounds(230,110,300,30);
				jtfEm.setFont(new Font("Arial", 0, 12));
				this.add(jtfEm);
				
				JLabel labeljtfTfn = new JLabel("Telefono:");
				labeljtfTfn.setBounds(100,160,120,30);
				labeljtfTfn.setFont(new Font("Arial", 1, 12));
				this.add(labeljtfTfn);
				
				JTextField jtfTfn = new JTextField();
				jtfTfn.setBounds(230,160,300,30);
				jtfTfn.setFont(new Font("Arial", 0, 12));
				this.add(jtfTfn);
				
				JButton boton = new JButton("OK");
				boton.setBounds(250,220,100,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(400,220,100,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
				
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					ResultSet resultado;

					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						String query = "INSERT INTO USUARIO VALUES ('" + cifrado.cifrar(jtf.getText())+"', '" + cifrado.hashear(jpf.getText())+"', '" + cifrado.cifrar(jtfEm.getText())+"', '" + cifrado.cifrar(jtfTfn.getText())+ "');";
							try {
								sentencia=this.conexion.prepareStatement(query);
								sentencia.executeUpdate();
								JOptionPane.showMessageDialog(null, "La operacion se ha realizado con exito", "Informacion", JOptionPane.INFORMATION_MESSAGE);
								Inicio i = new Inicio();
								i.setVisible(true);
								dispose();
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
					}
				});
				
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo insertar
		RegistroCon rc = new RegistroCon();
		rc.setVisible(true);
	}
}
