package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Insertar extends JFrame {
	//Constructor de la clase insertar
	public Insertar() {
		this.setTitle("Insertar");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		insert();
	}
	
	public void insert() {
		//colocar los componentes en la ventana
		JLabel labeljtf = new JLabel("�Donde desea insertar datos?");
		labeljtf.setBounds(240,10,280,30);
		labeljtf.setFont(new Font("Arial", 1, 12));
		this.add(labeljtf);
		
		String elementos[] = {"","Empleados","Tiendas","Usuarios"};
		
		JComboBox jcb = new JComboBox(elementos);
		jcb.setBounds(220,60,210,30);
		jcb.setFont(new Font("Arial", 0, 12));
		this.add(jcb);
		
		JButton boton = new JButton("OK");
		boton.setBounds(220,120,100,30);
		boton.setFont(new Font("Arial", 0, 12));
		this.add(boton);
		
		JButton botonCanc = new JButton("Cancelar");
		botonCanc.setBounds(330,120,100,30);
		botonCanc.setFont(new Font("Arial", 0, 12));
		this.add(botonCanc);
		
		//escuchador
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jcb.getSelectedItem()=="Empleados") {
					InsertarEmp insertE = new InsertarEmp();
					insertE.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Tiendas") {
					InsertarTi insertT = new InsertarTi();
					insertT.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Usuarios") {
					InsertarUs insertU = new InsertarUs();
					insertU.setVisible(true);
					dispose();
				}
			}
		});
		
		botonCanc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo insertar
		Insertar insert = new Insertar();
		insert.setVisible(true);
	}
}
