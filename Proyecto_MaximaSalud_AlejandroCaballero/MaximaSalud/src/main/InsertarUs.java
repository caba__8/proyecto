package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class InsertarUs extends JFrame {
	
	LibreriaCifrado cifrado;
	
	//Constructor de la clase insertar usuario
	public InsertarUs() {
		this.setTitle("Insertar usuario");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		insertU();
	}
	
	public void insertU() {
		cifrado = new LibreriaCifrado();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				//colocar los componentes en la ventana
				JLabel labelnom = new JLabel("Nombre:");
				labelnom.setBounds(70,10,200,30);
				labelnom.setFont(new Font("Arial", 1, 12));
				this.add(labelnom);
				
				JTextField jtf = new JTextField();
				jtf.setBounds(210,10,360,30);
				jtf.setFont(new Font("Arial", 0, 12));
				this.add(jtf);
				
				JLabel labelpw = new JLabel("Contraseņa:");
				labelpw.setBounds(70,60,200,30);
				labelpw.setFont(new Font("Arial", 1, 12));
				this.add(labelpw);
				
				JPasswordField jtfpw = new JPasswordField();
				jtfpw.setBounds(210,60,360,30);
				jtfpw.setFont(new Font("Arial", 0, 12));
				this.add(jtfpw);
				
				JLabel labelemail = new JLabel("Email:");
				labelemail.setBounds(70,110,200,30);
				labelemail.setFont(new Font("Arial", 1, 12));
				this.add(labelemail);
				
				JTextField jtfem = new JTextField();
				jtfem.setBounds(210,110,360,30);
				jtfem.setFont(new Font("Arial", 0, 12));
				this.add(jtfem);
				
				JLabel labeltel = new JLabel("Telefono:");
				labeltel.setBounds(70,160,200,30);
				labeltel.setFont(new Font("Arial", 1, 12));
				this.add(labeltel);
				
				JTextField jtftfn = new JTextField();
				jtftfn.setBounds(210,160,360,30);
				jtftfn.setFont(new Font("Arial", 0, 12));
				this.add(jtftfn);
				
				JButton boton = new JButton("OK");
				boton.setBounds(260,220,100,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(410,220,100,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
				
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					
					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						String query = "INSERT INTO USUARIO VALUES ('" + cifrado.cifrar(jtf.getText())+"', '" + cifrado.hashear(jtfpw.getText())+"', '" + cifrado.cifrar(jtfem.getText())+"', '" +cifrado.cifrar(jtftfn.getText())+"');";
						
						try {
							sentencia=this.conexion.prepareStatement(query);
							sentencia.executeUpdate();
							JOptionPane.showMessageDialog(null, "La operacion se ha realizado con exito", "Informacion", JOptionPane.INFORMATION_MESSAGE);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				});
				
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});	
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo insertar
		InsertarUs insertUser = new InsertarUs();
		insertUser.setVisible(true);
	}
}