package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class EliminarEmp extends JFrame {
	//Constructor de la clase eliminar empleado
	public EliminarEmp() {
		this.setTitle("Eliminar empleado");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		elimE();
	}
	
	public void elimE() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				JLabel labelnom = new JLabel("Introduce el dni del empleado que quieres eliminar: ");
				labelnom.setBounds(170,10,350,30);
				labelnom.setFont(new Font("Arial", 1, 12));
				this.add(labelnom);
				
				JTextField jtfnom = new JTextField();
				jtfnom.setBounds(150,60,350,30);
				jtfnom.setFont(new Font("Arial", 0, 12));
				this.add(jtfnom);
				
				JButton boton = new JButton("OK");
				boton.setBounds(200,120,100,30);
				boton.setFont(new Font("Arial", 0, 12));
				this.add(boton);
				
				JButton botonCanc = new JButton("Cancelar");
				botonCanc.setBounds(350,120,100,30);
				botonCanc.setFont(new Font("Arial", 0, 12));
				this.add(botonCanc);
		
				//escuchador
				boton.addActionListener(new ActionListener() {
					//conectamos con la base de datos
					Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/maximaSalud", "root", "");
					Statement stmt = conexion.createStatement();
					
					@Override
					public void actionPerformed(ActionEvent e) {
						PreparedStatement sentencia=null;
						System.out.println(jtfnom.getText());
						String query= "DELETE FROM empleados WHERE dniE= '"+jtfnom.getText() +"';";
						
						try {
							sentencia= this.conexion.prepareStatement(query);
							sentencia.executeUpdate();
							JOptionPane.showMessageDialog(null, "La operacion se ha realizado con exito", "Informacion", JOptionPane.INFORMATION_MESSAGE);
						} catch (SQLException ex) {
							ex.printStackTrace();
						}
					}
				});
		
				botonCanc.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);
					}
				});
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo eliminar empleado
		EliminarEmp elE = new EliminarEmp();
		elE.setVisible(true);
	}
}