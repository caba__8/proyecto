package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AccionA extends JFrame {
	//Constructor de la clase accion edicion
	public AccionA() {
		this.setTitle("Accion administrador");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		actA();
	}
	
	public void actA() {
		//colocar los componentes en la ventana
		JLabel labeljtf = new JLabel("�Que accion desea realizar sobre los usuarios?");
		labeljtf.setBounds(190,10,280,30);
		labeljtf.setFont(new Font("Arial", 1, 12));
		this.add(labeljtf);
		
		String elementos[] = {"","Insertar","Modificar","Eliminar"};
		
		JComboBox jcb = new JComboBox(elementos);
		jcb.setBounds(220,60,210,30);
		jcb.setFont(new Font("Arial", 0, 12));
		this.add(jcb);
		
		JButton boton = new JButton("OK");
		boton.setBounds(220,120,100,30);
		boton.setFont(new Font("Arial", 0, 12));
		this.add(boton);
		
		JButton botonCanc = new JButton("Cancelar");
		botonCanc.setBounds(330,120,100,30);
		botonCanc.setFont(new Font("Arial", 0, 12));
		this.add(botonCanc);
		
		//escuchador
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jcb.getSelectedItem()=="Insertar") {
					InsertarUs insertUser = new InsertarUs();
					insertUser.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Modificar") {
					ModificarUs modifUser = new ModificarUs();
					modifUser.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Eliminar") {
					EliminarUs elU = new EliminarUs();
					elU.setVisible(true);
					dispose();
				}
			}
		});
		
		botonCanc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}
	
	private void EnviarError() {
        System.out.println("Error");
    }
	
	public static void main(String[] args) {
		//Se crea una nueva accion
		AccionA aA = new AccionA();
		aA.setVisible(true);
	}
}
