package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import main.Consulta.ReportGeneratorEmpleados;
import main.Consulta.ReportGeneratorProductos;
import main.Consulta.ReportGeneratorTiendas;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class PInicial extends JFrame {
	
	public PInicial() {
		setPanel();
		//init();
	}
	
	private void setPanel() {
		this.setTitle("Inicio");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setLayout(null);
		iSesion();
	}
	
	/*private void init() {
		iSesion();
	}*/
	
	public void iSesion() {
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(40,0,600,450);
		
		ImageIcon i = new ImageIcon(getClass().getResource("logo.JPG"));
		ImageIcon img = new ImageIcon(i.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_SMOOTH));
		
		lblNewLabel.setIcon(img);
		this.getContentPane().add(lblNewLabel);
		
		JButton boton = new JButton("Entrar");
		boton.setBounds(185,470,100,30);
		boton.setFont(new Font("Arial", 0, 12));
		this.add(boton);
		
		JButton botonCanc = new JButton("Salir");
		botonCanc.setBounds(370,470,100,30);
		botonCanc.setFont(new Font("Arial", 0, 12));
		this.add(botonCanc);
		
		//escuchador
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Inicio i = new Inicio();
				i.setVisible(true);
				dispose();
			}
		});
		
		botonCanc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}
	
	public static void main(String[] args) {
		//Se crea un nuevo cliente
		PInicial i = new PInicial();
		i.setVisible(true);
	}
}
