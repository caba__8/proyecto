package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AccionE extends JFrame {
	//Constructor de la clase accion edicion
	public AccionE() {
		this.setTitle("Accion edicion");
		this.setSize(700, 600);
		this.getContentPane().setBackground(new Color(110, 241, 68));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		actE();
	}
	
	public void actE() {
		//colocar los componentes en la ventana
		JLabel labeljtf = new JLabel("�Que accion desea realizar?");
		labeljtf.setBounds(240,10,280,30);
		labeljtf.setFont(new Font("Arial", 1, 12));
		this.add(labeljtf);
		
		String elementos[] = {"","Visualizar","Insertar","Modificar","Eliminar", "Registro completo de datos"};
		
		JComboBox jcb = new JComboBox(elementos);
		jcb.setBounds(220,60,210,30);
		jcb.setFont(new Font("Arial", 0, 12));
		this.add(jcb);
		
		JButton boton = new JButton("OK");
		boton.setBounds(220,120,100,30);
		boton.setFont(new Font("Arial", 0, 12));
		this.add(boton);
		
		JButton botonCanc = new JButton("Cancelar");
		botonCanc.setBounds(330,120,100,30);
		botonCanc.setFont(new Font("Arial", 0, 12));
		this.add(botonCanc);
		
		//escuchador
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jcb.getSelectedItem()=="Visualizar") {
					VisualizarE ve = new VisualizarE();
					ve.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Insertar") {
					Insertar insert = new Insertar();
					insert.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Modificar") {
					Modificar m = new Modificar();
					m.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Eliminar") {
					Eliminar el = new Eliminar();
					el.setVisible(true);
					dispose();
				}
				if (jcb.getSelectedItem()=="Registro completo de datos") {
					try {
						Runtime.getRuntime().exec("java -jar maximaSalud_jar\\maximaSalud.jar");
					} catch (Exception e1) {            
						System.out.println(e1);
					}
					dispose();
				}
			}
		});
		
		botonCanc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}
	
	private void EnviarError() {
        System.out.println("Error");
    }
	
	public static void main(String[] args) {
		//Se crea una nueva accion
		AccionE aE = new AccionE();
		aE.setVisible(true);
	}
}
