package clases;

import java.util.Date;

public class Cliente {
	private int idcliente;
	private String nombre;
	private String apellidos;
	private String dni;
	private String fechanacimiento;
	private String fechacompra;
	private String ciudad;
	private String pais;
	
	public Cliente(int idcliente, String nombre, String apellidos, String dni, String fechanacimiento, String fechacompra, String ciudad, String pais) {
		super();
		this.idcliente = idcliente;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		this.fechanacimiento = fechanacimiento;
		this.fechacompra = fechacompra;
		this.ciudad = ciudad;
		this.pais = pais;
	}
	
	public Cliente(String dni) {
		super();
		this.dni = dni;
	}

	public int getidcliente() {
		return idcliente;
	}
	
	public void setidcliente(int idcliente) {
		this.idcliente = idcliente;
	}
	
	public String getnombre() {
		return nombre;
	}
	
	public void setnombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getapellidos() {
		return apellidos;
	}
	
	public void setapellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getdni() {
		return dni;
	}
	
	public void setdni(String dni) {
		this.dni = dni;
	}
	
	public String getfechanacimiento() {
		return fechanacimiento;
	}
	
	public void setfechanacimiento(String fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}
	
	public String getfechacompra() {
		return fechacompra;
	}
	
	public void setfechacompra(String fechacompra) {
		this.fechacompra = fechacompra;
	}
	
	public String getciudad() {
		return ciudad;
	}
	
	public void setciudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public String getpais() {
		return pais;
	}
	
	public void setpais(String pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Cliente [idCliente=" + idcliente + ", nombreCliente=" + nombre + ", apellido=" + apellidos 
		+ ", DNI=" + dni + ", fechaNacimiento=" + fechanacimiento + ", fechaCompra=" + fechacompra
		+ ", ciudad=" + ciudad + ", pais=" + pais +"]";
	}
}