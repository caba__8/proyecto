package clases;

public class Compra {
	private int idcompra;
	private String codigoCompra;
	private int idcliente;
	private int idtienda;
	private int idproducto;
	private int idempleado;
	private int cantidadCompra;
	
	public Compra(int idcompra, String codigoCompra, int idcliente, int idtienda, int idproducto, int idempleado, int cantidadCompra) {
		super();
		this.idcompra = idcompra;
		this.codigoCompra = codigoCompra;
		this.idcliente = idcliente;
		this.idtienda = idtienda;
		this.idproducto = idproducto;
		this.idempleado = idempleado;
		this.cantidadCompra = cantidadCompra;
	}

	public Compra(String CODIGO_C) {
		super();
		this.codigoCompra = CODIGO_C;
	}
	
	public int getIdcompra() {
		return idcompra;
	}
	
	public void setIdcompra(int idcompra) {
		this.idcompra = idcompra;
	}

	public String getCodigoCompra() {
		return codigoCompra;
	}

	public void setCodigoCompra(String codigoCompra) {
		this.codigoCompra = codigoCompra;
	}

	public int getIdcliente() {
		return idcliente;
	}

	public void setIdcliente(int idcliente) {
		this.idcliente = idcliente;
	}

	public int getIdtienda() {
		return idtienda;
	}

	public void setIdtienda(int idtienda) {
		this.idtienda = idtienda;
	}

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public int getIdempleado() {
		return idempleado;
	}

	public void setIdempleado(int idempleado) {
		this.idempleado = idempleado;
	}

	public int getCantidadCompra() {
		return cantidadCompra;
	}

	public void setCantidadCompra(int cantidadCompra) {
		this.cantidadCompra = cantidadCompra;
	}

	@Override
	public String toString() {
		return "Compra [idcompra=" + idcompra + ", codigoCompra=" + codigoCompra + ", idcliente=" + idcliente
				+ ", idtienda=" + idtienda + ", idproducto=" + idproducto + ", idempleado=" + idempleado
				+ ", cantidadCompra=" + cantidadCompra + "]";
	}	
}
