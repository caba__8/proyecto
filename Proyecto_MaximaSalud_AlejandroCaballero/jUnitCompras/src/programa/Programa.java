package programa;

import clases.Cliente;
import clases.Compra;
import clases.Empleado;
import clases.Tienda;
import clases.Producto;

import java.util.Date;

import clases.GestorCompras;

public class Programa {
	
	public static void main(String[] args) {
		GestorCompras gestor = new GestorCompras();
		Compra compra = new Compra(1, "IBPJJ08", 1, 1, 1, 1, 12);
		Compra compra1 = new Compra(2, "FLMFA14", 8, 1, 2, 1, 10);
		Tienda tienda = new Tienda(1, "Farma", "farma@gmail.com", 612354879, "2021-03-04", "Buena", "farma.com", "Valencia");
		Tienda tienda1 = new Tienda(2, "Altea", "altea@hotmail.com", 613284597, "2022-03-04", "Regular", "altea.es", "Barcelona");
		Tienda tienda2 = new Tienda(3, "Teva", "teva@hotmail.com", 654987321, "2021-08-04", "Mala", "teva.com", "Zaragoza");
		Producto producto = new Producto(1, "Ibuprofeno", "IBP", "2021-03-04", "2024-03-04", 5.00f, 100, 1, 1);
		Producto producto1 = new Producto(2, "Flumil", "FLM", "2021-03-05", "2024-03-04", 4.00f, 150, 2, 2);
		Producto producto2 = new Producto(3, "Paracetamol", "PCT", "2021-03-03", "2024-03-04", 6.00f, 200, 3, 3);
		Empleado empleado = new Empleado(1, "Jorge", "Pombo", "31246578P", "1995-02-09", 1, 1, "Espania", 1250.50f);
		Empleado empleado1 = new Empleado(2, "Sergio", "Canales", "76451238C", "1994-02-10", 2, 2, "Espania", 1575f);
		Empleado empleado2 = new Empleado(3, "Carlos", "Sainz", "55221325C", "1996-02-28", 3, 3, "Espania", 2150.44f);
		Cliente cliente = new Cliente(1, "Jose", "Juan", "77135722R", "1995-02-09", "2022-02-09", "Zaragoza", "Espania");
		Cliente cliente1 = new Cliente(2, "Sergio", "Perez", "22113254T", "1998-08-09", "2022-02-06", "Valencia", "Espania");
		Cliente cliente2 = new Cliente(3, "Fernando", "Alonso", "99233212U", "1992-05-09", "2022-02-09", "Barcelona", "Espania");
		
		gestor.darDeAltaCompra(compra);
		gestor.darDeAltaCompra(compra1);
		
		gestor.listarCompras();
		
		gestor.existeCompra("FLMFA14");
		
		gestor.darDeAltaTienda(tienda);
		gestor.darDeAltaTienda(tienda1);
		gestor.darDeAltaTienda(tienda2);
		
		gestor.listarTiendas();
		
		gestor.existeTienda("Farma");
		
		gestor.darDeAltaProducto(producto);
		gestor.darDeAltaProducto(producto1);
		gestor.darDeAltaProducto(producto2);
		
		gestor.listarProductos();
		
		gestor.existeProducto("PCT");
		
		gestor.darDeAltaEmpleado(empleado);
		gestor.darDeAltaEmpleado(empleado1);
		gestor.darDeAltaEmpleado(empleado2);
		
		gestor.listarEmpleados();
		
		gestor.existeEmpleado("76451238C");
		
		gestor.darDeAltaCliente(cliente);
		gestor.darDeAltaCliente(cliente1);
		gestor.darDeAltaCliente(cliente2);
		
		gestor.listarClientes();
		
		gestor.existeCliente("76451238C");
	}
}