package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorCompras;

public class ClienteTest {
static GestorCompras gestor;
	
	@BeforeAll 
	public static void setUpBeforeAll() {
		gestor= new GestorCompras();
	}
	
	@BeforeEach
	public void setUpBeforeTest() {
		gestor.getListaClientes().clear();
	}
	
	@Test
	public void altaClienteSinClientes() {
		Cliente clientes=new Cliente("56878123T");
		gestor.darDeAltaCliente(clientes);
		
		assertTrue(gestor.getListaClientes().contains(clientes));
	}
	
	@Test
	public void altaClienteConMasClientes() {
		Cliente cliente1 = new Cliente("46445345Q");
		gestor.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("68845345R");
		gestor.getListaClientes().add(cliente2);
		
		Cliente nuevoCliente=new Cliente("46783482G");
		gestor.darDeAltaCliente(nuevoCliente);
		
		assertTrue(gestor.getListaClientes().contains(nuevoCliente));
	}
	
	@Test
	public void altaClienteNegativo() {
		Cliente cliente1 = new Cliente("32467853Q");
		gestor.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("62788316G");
		gestor.getListaClientes().add(cliente2);
		
		Cliente esperado=new Cliente("64736534P");
		gestor.existeCliente("64736534P");
		
		boolean resultado=gestor.getListaClientes().contains(esperado);
		
		assertFalse(resultado);
	}
}
