package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.GestorCompras;
import clases.Tienda;

public class TiendaTest {
static GestorCompras gestor;
	
	@BeforeAll 
	public static void setUpBeforeAll() {
		gestor= new GestorCompras();
	}
	
	@BeforeEach
	public void setUpBeforeTest() {
		gestor.getListaTiendas().clear();
	}
	
	@Test
	public void altaTiendaSinTiendas() {
		Tienda tiendas=new Tienda("Farma");
		gestor.darDeAltaTienda(tiendas);
		
		assertTrue(gestor.getListaTiendas().contains(tiendas));
	}
	
	@Test
	public void altaTiendaConMasTiendas() {
		Tienda tienda1 = new Tienda("Teva");
		gestor.getListaTiendas().add(tienda1);
		Tienda tienda2 = new Tienda("Pinilla");
		gestor.getListaTiendas().add(tienda2);
		
		Tienda nuevoTienda=new Tienda("CHeca");
		gestor.darDeAltaTienda(nuevoTienda);
		
		assertTrue(gestor.getListaTiendas().contains(nuevoTienda));
	}
	
	@Test
	public void altaTiendaNegativo() {
		Tienda tienda1 = new Tienda("Molina");
		gestor.getListaTiendas().add(tienda1);
		Tienda tienda2 = new Tienda("Altea");
		gestor.getListaTiendas().add(tienda2);
		
		Tienda esperado=new Tienda("Megina");
		gestor.existeTienda("Megina");
		
		boolean resultado=gestor.getListaTiendas().contains(esperado);
		
		assertFalse(resultado);
	}
}

