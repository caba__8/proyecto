package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Compra;
import clases.GestorCompras;

public class CompraTest {
static GestorCompras gestor;
	
	@BeforeAll 
	public static void setUpBeforeAll() {
		gestor= new GestorCompras();
	}
	
	@BeforeEach
	public void setUpBeforeTest() {
		gestor.getListaCompras().clear();
	}
	
	@Test
	public void altaCompraSinCompras() {
		Compra compras=new Compra("IBPJJ08");
		gestor.darDeAltaCompra(compras);
		
		assertTrue(gestor.getListaCompras().contains(compras));
	}
	
	@Test
	public void altaCompraConMasCompras() {
		Compra compra1 = new Compra("FLMFA14");
		gestor.getListaCompras().add(compra1);
		
		Compra nuevoCompra=new Compra("PCTFA10");
		gestor.darDeAltaCompra(nuevoCompra);
		
		assertTrue(gestor.getListaCompras().contains(nuevoCompra));
	}
	
	@Test
	public void altaCompraNegativo() {
		Compra compra1 = new Compra("EBSSC10");
		gestor.getListaCompras().add(compra1);
		
		Compra esperado=new Compra("IBPCS55");
		gestor.existeCompra("IBPCS55");
		
		boolean resultado=gestor.getListaCompras().contains(esperado);
		
		assertFalse(resultado);
	}
}
