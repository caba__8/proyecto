package clases;

import java.util.Date;

public class Tienda {
	private int idtienda;
	private String tienda;
	private String fechacreacion;
	private String reputacion;
	private String email;
	private int telefono;
	private String web;
	private String ciudad;
	
	public Tienda(int idtienda, String tienda, String email, int telefono, String fechacreacion, String reputacion, String web, String ciudad) {
		super();
		this.idtienda = idtienda;
		this.tienda = tienda;
		this.fechacreacion = fechacreacion;
		this.reputacion = reputacion;
		this.email = email;
		this.telefono = telefono;
		this.web = web;
		this.ciudad = ciudad;
	}
	
	public Tienda(String tienda) {
		super();
		this.tienda = tienda;
	}
	
	public int getidtienda() {
		return idtienda;
	}
	
	public void setidtienda(int idtienda) {
		this.idtienda = idtienda;
	}
	
	public String gettienda() {
		return tienda;
	}
	
	public void settienda(String tienda) {
		this.tienda = tienda;
	}
	
	public String getfechacreacion() {
		return fechacreacion;
	}
	
	public void setfechacreacion(String fechacreacion) {
		this.fechacreacion = fechacreacion;
	}
	
	public String getreputacion() {
		return reputacion;
	}
	
	public void setreputacion(String reputacion) {
		this.reputacion = reputacion;
	}
	
	public String getemail() {
		return email;
	}
	
	public void setemail(String email) {
		this.email = email;
	}
	
	public int gettelefono() {
		return telefono;
	}
	
	public void settelefono(int telefono) {
		this.telefono = telefono;
	}
	
	public String getweb() {
		return web;
	}
	
	public void setweb(String web) {
		this.web = web;
	}
	
	public String getciudad() {
		return ciudad;
	}
	
	public void setciudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Override
	public String toString() {
		return "Tienda [idTienda=" + idtienda + ", Tienda=" + tienda + ", fechaCreacion=" + fechacreacion + ", reputacion=" + reputacion + 
		", email=" + email + ", telefono=" + telefono + ", web=" + web + ", ciudad=" + ciudad + "]";
	}
}