package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorTiendas {
	private ArrayList<Tienda> listaTiendas;
	private ArrayList<Empleado> listaEmpleados;
	private ArrayList<Producto> listaProductos;
	
	public GestorTiendas() {
		listaTiendas = new ArrayList<Tienda>();
		listaEmpleados = new ArrayList<Empleado>();
		listaProductos = new ArrayList<Producto>();
	}
	
	public ArrayList<Producto> getListaProductos() {
		return listaProductos;
	}
	
	public ArrayList<Empleado> getListaEmpleados() {
		return listaEmpleados;
	}

	public ArrayList<Tienda> getListaTiendas() {
		return listaTiendas;
	}
	
	public void darDeAltaTienda(Tienda tienda) {
		String tienda2 = tienda.gettienda();
		for(Tienda tiend : listaTiendas){
			if(tiend.gettienda().equals(tienda2)){
				return;
			}
		}
		listaTiendas.add(tienda);
	}
	
	public void listarTiendas() {
		for (Tienda tienda : listaTiendas) {
			if (tienda != null) {
				System.out.println(tienda);
			}
		}
	}
	
	public Tienda buscarTienda(String tienda2) {
		for (Tienda tienda : listaTiendas) {
			if (tienda != null && tienda.gettienda().equals(tienda2)) {
				return tienda;
			}
		}
		return null;
	}
	
	public boolean existeTienda(String tienda2) {
		for (Tienda tienda : listaTiendas) {
			if (tienda != null && tienda.gettienda().equals(tienda2)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaTienda(String tienda2) {
		Iterator<Tienda> iteradorTiendas = listaTiendas.iterator();		
		while (iteradorTiendas.hasNext()) {
			Tienda tiendas = iteradorTiendas.next();
			if (tiendas.gettienda().equals(tienda2)) {
				iteradorTiendas.remove();
			}
		}
	}
	
	public void darDeAltaEmpleado(Empleado empleado) {
		String dniE = empleado.getdniE();
		for(Empleado emplead : listaEmpleados){
			if(emplead.getdniE().equals(dniE)){
				return;
			}
		}
		listaEmpleados.add(empleado);
	}
	
	public void listarEmpleados() {
		for (Empleado empleado : listaEmpleados) {
			if (empleado != null) {
				System.out.println(empleado);
			}
		}
	}
	
	public Empleado buscarEmpleado(String dniE) {
		for (Empleado empleado : listaEmpleados) {
			if (empleado != null && empleado.getdniE().equals(dniE)) {
				return empleado;
			}
		}
		return null;
	}
	
	public boolean existeEmpleado(String dniE) {
		for (Empleado empleado : listaEmpleados) {
			if (empleado != null && empleado.getdniE().equals(dniE)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaEmpleado(String dniE) {
		Iterator<Empleado> iteradorEmpleados = listaEmpleados.iterator();		
		while (iteradorEmpleados.hasNext()) {
			Empleado empleados = iteradorEmpleados.next();
			if (empleados.getdniE().equals(dniE)) {
				iteradorEmpleados.remove();
			}	
		}
	}
	
	public void darDeAltaProducto(Producto producto) {
		String codigo = producto.getcodigo();
		for(Producto product : listaProductos){
			if(product.getcodigo().equals(codigo)){
				return;
			}
		}
		listaProductos.add(producto);
	}
	
	public void listarProductos() {
		for (Producto producto : listaProductos) {
			if (producto != null) {
				System.out.println(producto);
			}
		}
	}
	
	public Producto buscarProducto(String codigo) {
		for (Producto producto : listaProductos) {
			if (producto != null && producto.getcodigo().equals(codigo)) {
				return producto;
			}
		}
		return null;
	}
	
	public boolean existeProducto(String codigo) {
		for (Producto producto : listaProductos) {
			if (producto != null && producto.getcodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaProducto(String codigo) {
		Iterator<Producto> iteradorProductos = listaProductos.iterator();		
		while (iteradorProductos.hasNext()) {
			Producto productos = iteradorProductos.next();
			if (productos.getcodigo().equals(codigo)) {
				iteradorProductos.remove();
			}	
		}
	}
	
}