package clases;

import java.util.Date;

public class Empleado {
	private int idempleado;
	private String nombreE;
	private String apellidosE;
	private String dniE;
	private String fechanacimientoE;
	private int idproducto;
	private int idtienda;
	private String paisE;
	private float sueldoE;
	
	public Empleado(int idempleado, String nombreE, String apellidosE, String dniE, String fechanacimientoE, int idproducto, int idtienda, String paisE, float sueldoE) {
		super();
		this.idempleado = idempleado;
		this.nombreE = nombreE;
		this.apellidosE = apellidosE;
		this.dniE = dniE;
		this.fechanacimientoE = fechanacimientoE;
		this.idproducto = idproducto;
		this.idtienda = idtienda;
		this.paisE = paisE;
		this.sueldoE = sueldoE;
	}
	
	public Empleado(String CODIGO_EMP) {
		super();
		this.dniE = CODIGO_EMP;
	}
	
	public int getidempleado() {
		return idempleado;
	}
	
	public void setidempleado(int idempleado) {
		this.idempleado = idempleado;
	}
	
	public String getnombreE() {
		return nombreE;
	}
	
	public void setnombreE(String nombreE) {
		this.nombreE = nombreE;
	}
	
	public String getapellidosE() {
		return apellidosE;
	}
	
	public void setapellidosE(String apellidosE) {
		this.apellidosE = apellidosE;
	}
	
	public String getdniE() {
		return dniE;
	}
	
	public void setdniE(String dniE) {
		this.dniE = dniE;
	}
	
	public String getfechanacimientoE() {
		return fechanacimientoE;
	}
	
	public void setfechanacimientoE(String fechanacimientoE) {
		this.fechanacimientoE = fechanacimientoE;
	}
	
	public int getidproducto() {
		return idproducto;
	}
	
	public void setidproducto(int idproducto) {
		this.idproducto = idproducto;
	}
	
	public int getidtienda() {
		return idtienda;
	}
	
	public void setidtienda(int idtienda) {
		this.idtienda = idtienda;
	}
	
	public String getpaisE() {
		return paisE;
	}
	
	public void setpaisE(String paisE) {
		this.paisE = paisE;
	}
	
	public Float getsueldoE() {
		return sueldoE;
	}
	
	public void setsueldoE(Float sueldoE) {
		this.sueldoE = sueldoE;
	}

	@Override
	public String toString() {
		return "Empleado [idEmpleado=" + idempleado + ", nombreEmpleado=" + nombreE + ", apellido=" + apellidosE + ", dni=" + dniE + 
		", fechaNacimiento=" + fechanacimientoE + ", producto=" + idproducto + ", tienda=" + idtienda 
		+ ", pais=" + paisE + ", sueldo=" + sueldoE + "]";
	}
}