package programa;

import clases.Empleado;
import clases.Tienda;
import clases.Producto;

import java.util.Date;

import clases.GestorTiendas;

public class Programa {
	
	public static void main(String[] args) {
		GestorTiendas gestor = new GestorTiendas();
		Tienda tienda = new Tienda(1, "Farma", "farma@gmail.com", 612354879, "2021-03-04", "Buena", "farma.com", "Valencia");
		Tienda tienda1 = new Tienda(2, "Altea", "altea@hotmail.com", 613284597, "2022-03-04", "Regular", "altea.es", "Barcelona");
		Tienda tienda2 = new Tienda(3, "Teva", "teva@hotmail.com", 654987321, "2021-08-04", "Mala", "teva.com", "Zaragoza");
		Producto producto = new Producto(1, "Ibuprofeno", "IBP", "2021-03-04", "2024-03-04", 5.00f, 100, 1, 1);
		Producto producto1 = new Producto(2, "Flumil", "FLM", "2021-03-05", "2024-03-04", 4.00f, 150, 2, 2);
		Producto producto2 = new Producto(3, "Paracetamol", "PCT", "2021-03-03", "2024-03-04", 6.00f, 200, 3, 3);
		Empleado empleado = new Empleado(1, "Jorge", "Pombo", "31246578P", "1995-02-09", 1, 1, "Espania", 1250.50f);
		Empleado empleado1 = new Empleado(2, "Sergio", "Canales", "76451238C", "1994-02-10", 2, 2, "Espania", 1575f);
		Empleado empleado2 = new Empleado(3, "Carlos", "Sainz", "55221325C", "1996-02-28", 3, 3, "Espania", 2150.44f);
		
		gestor.darDeAltaTienda(tienda);
		gestor.darDeAltaTienda(tienda1);
		gestor.darDeAltaTienda(tienda2);
		
		gestor.listarTiendas();
		
		gestor.existeTienda("Farma");
		
		gestor.darDeAltaProducto(producto);
		gestor.darDeAltaProducto(producto1);
		gestor.darDeAltaProducto(producto2);
		
		gestor.listarProductos();
		
		gestor.existeProducto("PCT");
		
		gestor.darDeAltaEmpleado(empleado);
		gestor.darDeAltaEmpleado(empleado1);
		gestor.darDeAltaEmpleado(empleado2);
		
		gestor.listarEmpleados();
		
		gestor.existeEmpleado("76451238C");
	}
}