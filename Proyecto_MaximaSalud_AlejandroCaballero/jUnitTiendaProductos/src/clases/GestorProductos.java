package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorProductos {
	private ArrayList<Tienda> listaTiendas;
	private ArrayList<Producto> listaProductos;
	
	public GestorProductos() {
		listaTiendas = new ArrayList<Tienda>();
		listaProductos = new ArrayList<Producto>();
	}
	
	public ArrayList<Producto> getListaProductos() {
		return listaProductos;
	}

	public ArrayList<Tienda> getListaTiendas() {
		return listaTiendas;
	}
	
	public void darDeAltaTienda(Tienda tienda) {
		String tienda2 = tienda.gettienda();
		for(Tienda tiend : listaTiendas){
			if(tiend.gettienda().equals(tienda2)){
				return;
			}
		}
		listaTiendas.add(tienda);
	}
	
	public void listarTiendas() {
		for (Tienda tienda : listaTiendas) {
			if (tienda != null) {
				System.out.println(tienda);
			}
		}
	}
	
	public Tienda buscarTienda(String tienda2) {
		for (Tienda tienda : listaTiendas) {
			if (tienda != null && tienda.gettienda().equals(tienda2)) {
				return tienda;
			}
		}
		return null;
	}
	
	public boolean existeTienda(String tienda2) {
		for (Tienda tienda : listaTiendas) {
			if (tienda != null && tienda.gettienda().equals(tienda2)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaTienda(String tienda2) {
		Iterator<Tienda> iteradorTiendas = listaTiendas.iterator();		
		while (iteradorTiendas.hasNext()) {
			Tienda tiendas = iteradorTiendas.next();
			if (tiendas.gettienda().equals(tienda2)) {
				iteradorTiendas.remove();
			}
		}
	}
	
	public void darDeAltaProducto(Producto producto) {
		String codigo2 = producto.getcodigo();
		for(Producto product : listaProductos){
			if(product.getcodigo().equals(codigo2)){
				return;
			}
		}
		listaProductos.add(producto);
	}
	
	public void listarProductos() {
		for (Producto producto : listaProductos) {
			if (producto != null) {
				System.out.println(producto);
			}
		}
	}
	
	public Producto buscarProducto(String codigo2) {
		for (Producto producto : listaProductos) {
			if (producto != null && producto.getcodigo().equals(codigo2)) {
				return producto;
			}
		}
		return null;
	}
	
	public boolean existeProducto(String codigo2) {
		for (Producto producto : listaProductos) {
			if (producto != null && producto.getcodigo().equals(codigo2)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaProducto(String codigo2) {
		Iterator<Producto> iteradorProductos = listaProductos.iterator();		
		while (iteradorProductos.hasNext()) {
			Producto productos = iteradorProductos.next();
			if (productos.getcodigo().equals(codigo2)) {
				iteradorProductos.remove();
			}	
		}
	}
}