package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.GestorProductos;
import clases.Producto;

public class ProductoTest {
static GestorProductos gestor;
	
	@BeforeAll 
	public static void setUpBeforeAll() {
		gestor= new GestorProductos();
	}
	
	@BeforeEach
	public void setUpBeforeTest() {
		gestor.getListaProductos().clear();
	}
	
	@Test
	public void altaProductoSinProductos() {
		Producto productos=new Producto("67438HF");
		gestor.darDeAltaProducto(productos);
		
		assertTrue(gestor.getListaProductos().contains(productos));
	}
	
	@Test
	public void altaProductoConMasProductos() {
		Producto producto1 = new Producto("12345AB");
		gestor.getListaProductos().add(producto1);
		Producto producto2 = new Producto("46737CD");
		gestor.getListaProductos().add(producto2);
		
		Producto nuevaProducto=new Producto("56473BD");
		gestor.darDeAltaProducto(nuevaProducto);
		
		assertTrue(gestor.getListaProductos().contains(nuevaProducto));
	}
	
	@Test
	public void altaProductoNegativa() {
		Producto producto1 = new Producto("67465BN");
		gestor.getListaProductos().add(producto1);
		Producto producto2 = new Producto("67381PJ");
		gestor.getListaProductos().add(producto2);
		
		Producto esperada=new Producto("53628TY");
		gestor.existeProducto("53628TY");
		
		boolean resultado=gestor.getListaProductos().contains(esperada);
		
		assertFalse(resultado);
	}


}
