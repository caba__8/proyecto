package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.GestorProductos;
import clases.Tienda;

public class TiendaTest {
static GestorProductos gestor;
	
	@BeforeAll 
	public static void setUpBeforeAll() {
		gestor= new GestorProductos();
	}
	
	@BeforeEach
	public void setUpBeforeTest() {
		gestor.getListaTiendas().clear();
	}
	
	@Test
	public void altaTiendaSinTiendas() {
		Tienda tiendas=new Tienda("56878123T");
		gestor.darDeAltaTienda(tiendas);
		
		assertTrue(gestor.getListaTiendas().contains(tiendas));
	}
	
	@Test
	public void altaTiendaConMasTiendas() {
		Tienda tienda1 = new Tienda("46445345Q");
		gestor.getListaTiendas().add(tienda1);
		Tienda tienda2 = new Tienda("68845345R");
		gestor.getListaTiendas().add(tienda2);
		
		Tienda nuevoTienda=new Tienda("46783482G");
		gestor.darDeAltaTienda(nuevoTienda);
		
		assertTrue(gestor.getListaTiendas().contains(nuevoTienda));
	}
	
	@Test
	public void altaTiendaNegativo() {
		Tienda tienda1 = new Tienda("32467853Q");
		gestor.getListaTiendas().add(tienda1);
		Tienda tienda2 = new Tienda("62788316G");
		gestor.getListaTiendas().add(tienda2);
		
		Tienda esperado=new Tienda("64736534P");
		gestor.existeTienda("64736534P");
		
		boolean resultado=gestor.getListaTiendas().contains(esperado);
		
		assertFalse(resultado);
	}

}

