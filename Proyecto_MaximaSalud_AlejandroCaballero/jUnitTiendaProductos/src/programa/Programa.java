package programa;
import clases.Tienda;
import clases.Producto;

import java.util.Date;

import clases.GestorProductos;

public class Programa {
	
	public static void main(String[] args) {
		GestorProductos gestor = new GestorProductos();
		Tienda tienda = new Tienda(1, "Farma", "farma@gmail.com", 612354879, "2021-03-04", "Buena", "farma.com", "Valencia");
		Tienda tienda1 = new Tienda(2, "Altea", "altea@hotmail.com", 613284597, "2022-03-04", "Regular", "altea.es", "Barcelona");
		Tienda tienda2 = new Tienda(3, "Teva", "teva@hotmail.com", 654987321, "2021-08-04", "Mala", "teva.com", "Zaragoza");
		Producto producto = new Producto(1, "Ibuprofeno", "IBP", "2021-03-04", "2024-03-04", 5.00f, 100, 1, 1);
		Producto producto1 = new Producto(2, "Flumil", "FLM", "2021-03-05", "2024-03-04", 4.00f, 150, 2, 2);
		Producto producto2 = new Producto(3, "Paracetamol", "PCT", "2021-03-03", "2024-03-04", 6.00f, 200, 3, 3);
		
		gestor.darDeAltaTienda(tienda);
		gestor.darDeAltaTienda(tienda1);
		gestor.darDeAltaTienda(tienda2);
		
		gestor.listarTiendas();
		
		gestor.existeTienda("Farma");
		
		gestor.darDeAltaProducto(producto);
		gestor.darDeAltaProducto(producto1);
		gestor.darDeAltaProducto(producto2);
		
		gestor.listarProductos();
		
		gestor.existeProducto("PCT");
		
	}


}