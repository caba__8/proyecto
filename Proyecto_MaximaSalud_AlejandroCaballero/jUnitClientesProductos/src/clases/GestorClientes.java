package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorClientes {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Producto> listaProductos;
	
	public GestorClientes() {
		listaClientes = new ArrayList<Cliente>();
		listaProductos = new ArrayList<Producto>();
	}
	
	public ArrayList<Producto> getListaProductos() {
		return listaProductos;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
	public void darDeAltaCliente(Cliente cliente) {
		String dni = cliente.getdni();
		for(Cliente client : listaClientes){
			if(client.getdni().equals(dni)){
				return;
			}
		}
		listaClientes.add(cliente);
	}
	
	public void listarClientes() {
		for (Cliente cliente : listaClientes) {
			if (cliente != null) {
				System.out.println(cliente);
			}
		}
	}
	
	public Cliente buscarCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente != null && cliente.getdni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}
	
	public boolean existeCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente != null && cliente.getdni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaCliente(String dni) {
		Iterator<Cliente> iteradorClientes = listaClientes.iterator();		
		while (iteradorClientes.hasNext()) {
			Cliente clientes = iteradorClientes.next();
			if (clientes.getdni().equals(dni)) {
				iteradorClientes.remove();
			}
		}
	}
	
	public void darDeAltaProducto(Producto producto) {
		String codigo = producto.getcodigo();
		for(Producto product : listaProductos){
			if(product.getcodigo().equals(codigo)){
				return;
			}
		}
		listaProductos.add(producto);
	}
	
	public void listarProductos() {
		for (Producto producto : listaProductos) {
			if (producto != null) {
				System.out.println(producto);
			}
		}
	}
	
	public Producto buscarProducto(String codigo) {
		for (Producto producto : listaProductos) {
			if (producto != null && producto.getcodigo().equals(codigo)) {
				return producto;
			}
		}
		return null;
	}
	
	public boolean existeProducto(String codigo) {
		for (Producto producto : listaProductos) {
			if (producto != null && producto.getcodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	
	public void darDeBajaProducto(String codigo) {
		Iterator<Producto> iteradorProductos = listaProductos.iterator();		
		while (iteradorProductos.hasNext()) {
			Producto productos = iteradorProductos.next();
			if (productos.getcodigo().equals(codigo)) {
				iteradorProductos.remove();
			}	
		}
	}
}