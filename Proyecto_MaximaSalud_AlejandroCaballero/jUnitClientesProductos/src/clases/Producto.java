package clases;

import java.util.Date;

public class Producto {
	private int idproducto;
	private String genero;
	private String codigo;
	private String fechaedicion;
	private String fechacaducidad;
	private float precio;
	private int cantidad;
	private int idtienda;
	private int idcliente;
	
	public Producto(int idproducto, String genero, String codigo, String fechaedicion, String fechacaducidad, float precio, int cantidad, int idtienda, int idcliente) {
		super();
		this.idproducto = idproducto;
		this.genero = genero;
		this.codigo = codigo;
		this.fechaedicion = fechaedicion;
		this.fechacaducidad = fechacaducidad;
		this.precio = precio;
		this.cantidad = cantidad;
		this.idtienda = idtienda;
		this.idcliente = idcliente;
	}
	
	public Producto(String codigo) {
		super();
		this.codigo = codigo;
	}
	
	public int getidproducto() {
		return idproducto;
	}
	
	public void setidproducto(int idproducto) {
		this.idproducto = idproducto;
	}
	
	public String getgenero() {
		return genero;
	}
	
	public void setgenero(String genero) {
		this.genero = genero;
	}
	
	public String getcodigo() {
		return codigo;
	}
	
	public void setcodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getfechaedicion() {
		return fechaedicion;
	}
	
	public void setfechaedicion(String fechaedicion) {
		this.fechaedicion = fechaedicion;
	}
	
	public String getfechacaducidad() {
		return fechacaducidad;
	}
	
	public void setfechacaducidad(String fechacaducidad) {
		this.fechacaducidad = fechacaducidad;
	}
	
	public float getprecio() {
		return precio;
	}
	
	public void setprecio(float precio) {
		this.precio = precio;
	}
	
	public int getcantidad() {
		return cantidad;
	}
	
	public void setcantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public int getidtienda() {
		return idtienda;
	}
	
	public void setidtienda(int idtienda) {
		this.idtienda = idtienda;
	}
	
	public int getidcliente() {
		return idcliente;
	}
	
	public void setidcliente(int idcliente) {
		this.idcliente = idcliente;
	}

	@Override
	public String toString() {
		return "Producto [idProducto=" + idproducto + ", nombreProducto=" + genero + ", codigo=" + codigo + ", fechaDisenio=" + fechaedicion 
		+ ", fechaCaducidad=" + fechacaducidad + ", precio=" + precio + ", cantidad=" + cantidad + ", idTienda" + idtienda + ", idCliente" + idcliente +"]";
	}
}