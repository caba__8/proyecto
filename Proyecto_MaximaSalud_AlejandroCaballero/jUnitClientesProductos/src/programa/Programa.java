package programa;
import clases.Cliente;
import clases.GestorClientes;
import clases.Producto;

public class Programa {
	
	public static void main(String[] args) {
		GestorClientes gestor = new GestorClientes();
		Cliente cliente = new Cliente(1, "Jose", "Juan", "77135722R", "1995-02-09", "2022-02-09", "Zaragoza", "Espania");
		Cliente cliente1 = new Cliente(2, "Sergio", "Perez", "22113254T", "1998-08-09", "2022-02-06", "Valencia", "Espania");
		Cliente cliente2 = new Cliente(3, "Fernando", "Alonso", "99233212U", "1992-05-09", "2022-02-09", "Barcelona", "Espania");
		Producto producto = new Producto(1, "Ibuprofeno", "IBP", "2021-03-04", "2024-03-04", 5.00f, 100, 1, 1);
		Producto producto1 = new Producto(2, "Flumil", "FLM", "2021-03-05", "2024-03-04", 4.00f, 150, 2, 2);
		Producto producto2 = new Producto(3, "Paracetamol", "PCT", "2021-03-03", "2024-03-04", 6.00f, 200, 3, 3);
		
		gestor.darDeAltaCliente(cliente);
		gestor.darDeAltaCliente(cliente1);
		gestor.darDeAltaCliente(cliente2);
		
		gestor.listarClientes();
		
		gestor.existeCliente("22113254T");
		
		gestor.darDeAltaProducto(producto);
		gestor.darDeAltaProducto(producto1);
		gestor.darDeAltaProducto(producto2);
		
		gestor.listarProductos();
		
		gestor.existeProducto("PCT");
	}


}