package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Empleado;
import clases.GestorEmpleados;

public class EmpleadoTest {
static GestorEmpleados gestor;
	
	@BeforeAll 
	public static void setUpBeforeAll() {
		gestor= new GestorEmpleados();
	}
	
	@BeforeEach
	public void setUpBeforeTest() {
		gestor.getListaEmpleados().clear();
	}
	
	@Test
	public void altaEmpleadoSinEmpleados() {
		Empleado empleados=new Empleado("56878123T");
		gestor.darDeAltaEmpleado(empleados);
		
		assertTrue(gestor.getListaEmpleados().contains(empleados));
	}
	
	@Test
	public void altaEmpleadoConMasEmpleados() {
		Empleado empleado1 = new Empleado("46445345Q");
		gestor.getListaEmpleados().add(empleado1);
		Empleado empleado2 = new Empleado("68845345R");
		gestor.getListaEmpleados().add(empleado2);
		
		Empleado nuevoEmpleado=new Empleado("46783482G");
		gestor.darDeAltaEmpleado(nuevoEmpleado);
		
		assertTrue(gestor.getListaEmpleados().contains(nuevoEmpleado));
	}
	
	@Test
	public void altaEmpleadoNegativo() {
		Empleado empleado1 = new Empleado("32467853Q");
		gestor.getListaEmpleados().add(empleado1);
		Empleado empleado2 = new Empleado("62788316G");
		gestor.getListaEmpleados().add(empleado2);
		
		Empleado esperado=new Empleado("64736534P");
		gestor.existeEmpleado("64736534P");
		
		boolean resultado=gestor.getListaEmpleados().contains(esperado);
		
		assertFalse(resultado);
	}
}

